/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner.dbtype;

import cn.com.chiner.BaseStatic;
import cn.com.chiner.java.Application;
import org.junit.Test;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2024/1/24
 * @desc :
 */
public class HighGoMetaTest {
    @Test
    public void pingDriverLoadTest(){
        String[] args =  new String[]{
                "PingLoadDriverClass",                      //执行什么命令
                "driver_class_name=com.highgo.jdbc.Driver",
                "url=jdbc:highgo://192.168.3.119:5866/sims?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT8&useSSL=false",
                "username=sysdba",
                "password=Hello@1234",
                "out="+ BaseStatic.DIR_OUT+"/pdc-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }


    @Test
    public void listTableTest(){
        String[] args =  new String[]{
                "DBReverseGetAllTablesList",            //执行什么命令
                "driver_class_name=com.highgo.jdbc.Driver",
                "url=jdbc:highgo://192.168.3.119:5866/sims?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT8&useSSL=false&metaFromSQL=1&schemaOwner=mysql",
                "username=sysdba",
                "password=Hello@1234",
                "out="+ BaseStatic.DIR_OUT+"/dbrgatl-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }


    @Test
    public void getTableDDLTest(){
        String[] args =  new String[]{
                "DBReverseGetTableDDL",            //执行什么命令
                "driver_class_name=com.highgo.jdbc.Driver",
                "url=jdbc:highgo://192.168.3.119:5866/sims?metaFromSQL=1&schemaOwner=mysql&useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT8&useSSL=false",
                "username=sysdba",
                "password=Hello@1234",
                "tables=sims_student",
                "out="+ BaseStatic.DIR_OUT+"/dbrgtddl-"+System.nanoTime()+".json"
        };
        Application.main(args);
    }
}
