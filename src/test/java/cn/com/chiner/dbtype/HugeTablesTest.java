package cn.com.chiner.dbtype;

import cn.com.chiner.BaseStatic;
import cn.com.chiner.java.Application;
import cn.com.chiner.java.command.impl.PingLoadDriverClassImpl;
import cn.fisok.raw.kit.IOKit;
import cn.fisok.raw.kit.StringKit;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HugeTablesTest {
    String[] args =  new String[]{
            "PingLoadDriverClass",                      //执行什么命令
            "driver_class_name=com.microsoft.sqlserver.jdbc.SQLServerDriver",
            "url=jdbc:sqlserver://192.168.3.204:1433;databaseName=hugetables",
            "username=ca",
            "password=sqlserver*1433",
            "out="+ BaseStatic.DIR_OUT+"/ping-"+System.nanoTime()+".json"
    };
    @Test
    public void pingDriver(){
        Application.main(args);
    }

    @Test
    public void testAndExec() throws IOException, SQLException {
        InputStream in = getClass().getClassLoader().getResourceAsStream("sql/demo_person_company.sql");
        InputStreamReader reader = new InputStreamReader(in);
        String text = IOKit.toString(reader);
//        System.out.println(text);




        String cmdText = StringKit.nvl(args[0], "");
        List<String> argList = new ArrayList<String>();
        for (int i = 1; i < args.length; i++) {
            argList.add(args[i]);
        }
        Map<String, String> argsMap = Application.parseArgs(argList);

        PingLoadDriverClassImpl cmd = new PingLoadDriverClassImpl();
        cmd.exec(argsMap);
        Connection conn = cmd.createConnect();

        DecimalFormat df = new DecimalFormat("00000");
        for(int i=0;i<8000;i++){
            String suffix = df.format(i+1);
            String script = text.replaceAll("demo_person_company","demo_person_company_"+suffix);
            String[] cmdList = script.split("    GO");
            for(String cmdRow : cmdList){
                conn.createStatement().executeUpdate(cmdRow);
            }
            System.out.println(script+" [成功]");
        }
    }

}
