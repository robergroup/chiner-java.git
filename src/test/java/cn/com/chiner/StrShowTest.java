package cn.com.chiner;

import org.junit.Test;

public class StrShowTest {

    @Test
    public void test() {
        System.out.println(getQueryTableColumnsSQL());
    }

    protected String getQueryTableColumnsSQL() {
        return "SELECT\n" +
                "        tbl_name = d.name,\n" +
                "        tbl_comment = '',\n" +
                "        col_index = a.colorder,\n" +
                "        col_name = a.name,\n" +
                "        col_comment = CONVERT(nvarchar(1000),ISNULL(g.[value],'')),\n" +
                "        data_type = b.name,\n" +
                "        num_precision =\n" +
                "        CASE\n" +
                "            WHEN COLUMNPROPERTY(a.id, a.name, 'PRECISION' ) <= 0 THEN NULL ELSE COLUMNPROPERTY(a.id, a.name, 'PRECISION' )\n" +
                "            END,\n" +
                "        num_scale =\n" +
                "        CASE\n" +
                "            WHEN ISNULL(COLUMNPROPERTY(a.id, a.name, 'Scale'), 0 ) <= 0 THEN NULL ELSE ISNULL(COLUMNPROPERTY(a.id, a.name, 'Scale'), 0 )\n" +
                "            END,\n" +
                "        is_nullable =\n" +
                "        CASE\n" +
                "            WHEN a.isnullable = 1 THEN 'Y' ELSE ''\n" +
                "            END,\n" +
                "        is_primary_key = (\n" +
                "            SELECT\n" +
                "                'Y'\n" +
                "            FROM\n" +
                "                information_schema.table_constraints AS tc,\n" +
                "                information_schema.key_column_usage AS kcu\n" +
                "            WHERE tc.constraint_name = kcu.constraint_name\n" +
                "              AND tc.constraint_type = 'PRIMARY KEY'\n" +
                "              AND tc.table_name = d.name\n" +
                "              AND kcu.column_name=a.name\n" +
                "        ),\n" +
                "        default_value = ISNULL(e.text, '')\n" +
                "FROM\n" +
                "    syscolumns a\n" +
                "        INNER JOIN sysobjects d ON a.id= d.id AND d.xtype= 'U' AND d.name<> 'dtproperties'\n" +
                "        LEFT JOIN systypes b ON a.xusertype= b.xusertype\n" +
                "        LEFT JOIN syscomments e ON a.cdefault= e.id\n" +
                "        LEFT JOIN sys.extended_properties g ON a.id= g.major_id AND a.colid= g.minor_id\n" +
                "WHERE '1' <> ? AND upper(d.name)=upper(?)\n" +
                "ORDER BY\n" +
                "    a.id,\n" +
                "    a.colorder";
    }
}
