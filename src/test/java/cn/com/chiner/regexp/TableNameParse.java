package cn.com.chiner.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TableNameParse {
    public static void main(String[] args){

        parse("test1.xls#test-Sheet!B1");
        parse("test_Sheet!B1");
        parse("testSheet");

    }

    public static void parse(String text){
        String pattern = "(([A-Za-z0-9\\-_\\.]+[A-Za-z0-9\\-\\_\\.]{0,255}#)?)([A-Za-z0-9\\-_]+)((!\\w+)?)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(text);

        if (m.find()) {
            System.out.println(m.group(3));  // 输出 "testSheet"
        } else {
            System.out.println("No match found.");
        }
    }
}
