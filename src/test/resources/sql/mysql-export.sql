/*
 Navicat Premium Data Transfer

 Source Server         : local-mysql
 Source Server Type    : MySQL
 Source Server Version : 80100 (8.1.0)
 Source Host           : localhost:3306
 Source Schema         : crops

 Target Server Type    : MySQL
 Target Server Version : 80100 (8.1.0)
 File Encoding         : 65001

 Date: 17/10/2023 07:47:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for CUST_FNASTAT
-- ----------------------------
DROP TABLE IF EXISTS `CUST_FNASTAT`;
CREATE TABLE `CUST_FNASTAT` (
  `CUST_FNASTAT_ID` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '客户财报期次号',
  `CUST_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '客户ID',
  `CUST_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '客户名',
  `VERSION_NO` int DEFAULT NULL COMMENT '版本号',
  `FNASTAT_MODEL` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表模型',
  `FNASTAT_DATE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表截止日期',
  `BASE_YEAR` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '基准年份',
  `FNASTAT_PERIOD` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表周期',
  `FNASTAT_SCOPE` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表口径',
  `FNASTAT_CURRENCY` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表币种',
  `FNASTAT_CURRENCY_UNIT` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表单位',
  `FNASTAT_STATUS` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表状态',
  `REPORT_FLAG` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表检查标志',
  `REPORT_OPINION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表注释',
  `AUDIT_STATUS` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '是否审计',
  `AUDIT_UNIT` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审计单位',
  `AUDIT_FIRM_ID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审计会计事务所ID',
  `AUDIT_FIRM_NAME` varchar(90) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审计会计事务所名称',
  `AUDIT_OPINION_CHOOSE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审计意见选择',
  `AUDIT_OPINION_INTRO` varchar(900) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审计意见描述',
  `OTHER_AUDIT_FIRM_NAME` varchar(90) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '其他审计会计事务所名称',
  `EMPLOYEE_COUNT` int DEFAULT NULL COMMENT '员工人数',
  `FNASTAT_FORMAL_TYPE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '财报说明;正式财报，临时财报',
  `IS_EFFECT` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '是否有效',
  `REMARK` varchar(900) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  `IS_DRAFT` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '暂存草稿;YesNo',
  `IS_DELETE` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '是否删除;YesNo',
  `TENANT_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '租户号',
  `REVISION` int DEFAULT NULL COMMENT '乐观锁',
  `CREATED_USER_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人用户号',
  `CREATED_ORG_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人机构号',
  `CREATED_TIME` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `UPDATED_USER_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人用户号',
  `UPDATED_ORG_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人机构号',
  `UPDATED_TIME` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`CUST_FNASTAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='客户财务报表(期次）';

-- ----------------------------
-- Table structure for FISC_FNASTAT
-- ----------------------------
DROP TABLE IF EXISTS `FISC_FNASTAT`;
CREATE TABLE `FISC_FNASTAT` (
  `FNASTAT_ID` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '财务报表号',
  `OBJECT_TYPE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '对象类型',
  `OBJECT_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '对象编号',
  `UNIFIED_CODE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表统一识别码',
  `DEF_KEY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表定义',
  `FNASTAT_NAME` varchar(90) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '财务报表名称',
  `FNASTAT_DATE` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表日期',
  `FNASTAT_SCOPE` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表口径',
  `FNASTAT_PERIOD` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表周期',
  `CURRENCY` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表币种',
  `CURRENCY_UNIT` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表单位',
  `FNASTAT_STATUS` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '报表状态',
  `AUDIT_STATUS` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审计状态',
  `REMARK` varchar(900) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注说明',
  `FNASTAT_COLUMNS` varchar(1200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据列',
  `FNASTAT_COLUMN_STYLES` varchar(1200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据列样式',
  `IS_BALANCE` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '是否平衡;用于资产负债表',
  `IS_DRAFT` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '暂存草稿;YesNo',
  `IS_DELETE` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '是否删除;YesNo',
  `TENANT_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '租户号',
  `REVISION` int DEFAULT NULL COMMENT '乐观锁',
  `CREATED_USER_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人用户号',
  `CREATED_ORG_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人机构号',
  `CREATED_TIME` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `UPDATED_USER_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人用户号',
  `UPDATED_ORG_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人机构号',
  `UPDATED_TIME` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`FNASTAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='财务报表';

-- ----------------------------
-- Table structure for FISC_FNASTAT_SUBJECT
-- ----------------------------
DROP TABLE IF EXISTS `FISC_FNASTAT_SUBJECT`;
CREATE TABLE `FISC_FNASTAT_SUBJECT` (
  `UID` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '科目记录号',
  `FNASTAT_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '财务报表号',
  `SUBJECT_KEY` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '科目号',
  `SUBJECT_NAME` varchar(90) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '科目名',
  `ROW_NO` int DEFAULT NULL COMMENT '行次号',
  `UNIFIED_CODE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '科目统一识别码',
  `NUMBER_VALUE1` decimal(24,6) DEFAULT NULL COMMENT '数字值1',
  `NUMBER_VALUE2` decimal(24,6) DEFAULT NULL COMMENT '数字值2',
  `NUMBER_VALUE3` decimal(24,6) DEFAULT NULL COMMENT '数字值3',
  `NUMBER_VALUE4` decimal(24,6) DEFAULT NULL COMMENT '数字值4',
  `NUMBER_VALUE5` decimal(24,6) DEFAULT NULL COMMENT '数字值5',
  `NUMBER_VALUE6` decimal(24,6) DEFAULT NULL COMMENT '数字值6',
  `NUMBER_VALUE7` decimal(24,6) DEFAULT NULL COMMENT '数字值7',
  `NUMBER_VALUE8` decimal(24,6) DEFAULT NULL COMMENT '数字值8',
  `NUMBER_VALUE9` decimal(24,6) DEFAULT NULL COMMENT '数字值9',
  `NUMBER_VALUE10` decimal(24,6) DEFAULT NULL COMMENT '数字值10',
  `NUMBER_VALUE11` decimal(24,6) DEFAULT NULL COMMENT '数字值11',
  `NUMBER_VALUE12` decimal(24,6) DEFAULT NULL COMMENT '数字值12',
  `NUMBER_VALUE13` decimal(24,6) DEFAULT NULL COMMENT '数字值13',
  `NUMBER_VALUE14` decimal(24,6) DEFAULT NULL COMMENT '数字值14',
  `NUMBER_VALUE15` decimal(24,6) DEFAULT NULL COMMENT '数字值15',
  `NUMBER_VALUE16` decimal(24,6) DEFAULT NULL COMMENT '数字值16',
  `NUMBER_VALUE17` decimal(24,6) DEFAULT NULL COMMENT '数字值17',
  `STRING_VALUE1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值1;期末值',
  `STRING_VALUE2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值2',
  `STRING_VALUE3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值3',
  `STRING_VALUE4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值4',
  `STRING_VALUE5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值5',
  `STRING_VALUE6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值6',
  `STRING_VALUE7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值7',
  `STRING_VALUE8` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值8',
  `STRING_VALUE9` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字串值9',
  `STATUS` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '状态',
  `IS_DRAFT` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '暂存草稿;YesNo',
  `IS_DELETE` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '是否删除;YesNo',
  `TENANT_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '租户号',
  `REVISION` int DEFAULT NULL COMMENT '乐观锁',
  `CREATED_USER_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人用户号',
  `CREATED_ORG_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人机构号',
  `CREATED_TIME` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建时间',
  `UPDATED_USER_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人用户号',
  `UPDATED_ORG_ID` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人机构号',
  `UPDATED_TIME` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='科目明细;科目明细纵表，如果横表没有的字段，就补充到这里来';

SET FOREIGN_KEY_CHECKS = 1;
