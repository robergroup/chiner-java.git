select name as tbl_name,
       remarks as tbl_comment,
       creator as db_name
from sysibm.systables
where upper(creator)=upper('DB2INST1')
order by ctime asc;

select
    col.tbname as tbl_name,
    '' as tbl_comment,
    row_number() over() as col_index,
        col.name as col_name,
    remarks as col_comment,
    col.typename as data_type ,
    col.length as data_length,
    col.scale as num_scale,
    col.keyseq as is_primary_key,
    col.nulls as is_nullable,
    col.default as default_value
from sysibm.syscolumns col
where upper(tbcreator)=upper('DB2INST1')
    and tbname ='SIMS_STUDENT'
  ;