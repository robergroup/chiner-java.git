-- 使用数据库名
use db_name;

-- 1.机构类数据
-- 1.1机构信息
drop table if exists T_1_1;
create table T_1_1(
    A010001    varchar(24)      comment '机构ID'
   ,A010002    string           comment '内部机构号'
   ,A010003    varchar(15)      comment '金融许可证号'
   ,A010004    varchar(18)      comment '统一社会信用代码'
   ,A010005    varchar(200)     comment '银行机构名称'
   ,A010006    varchar(12)      comment '支付行号'
   ,A010007    varchar(2)       comment '机构类型'
   ,A010008    varchar(2)       comment '机构类别'
   ,A010009    varchar(1)       comment '县域机构标识'
   ,A010010    varchar(1)       comment '科技支行标识'
   ,A010011    varchar(1)       comment '科技特色支行标识'
   ,A010012    varchar(1)       comment '科技金融专营机构标识'
   ,A010013    varchar(6)       comment '行政区划'
   ,A010014    varchar(2)       comment '运营状态'
   ,A010015    varchar(10)      comment '成立日期'
   ,A010016    varchar(255)     comment '机构地址'
   ,A010017    varchar(200)     comment '负责人姓名'
   ,A010018    varchar(32)      comment '负责人工号'
   ,A010019    varchar(128)     comment '负责人联系电话'
   ,A010020    varchar(10)      comment '采集日期'
)
comment '1.1机构信息'
;

-- 1.2机构关系
drop table if exists T_1_2;
create table T_1_2(
     A020001 varchar(24) comment '机构ID'
    ,A020002 varchar(24) comment '上级管理机构ID'
    ,A020003 varchar(10) comment '采集日期'
)
comment '1.2机构关系'
;

-- 1.3员工
drop table if exists T_1_3;
create table T_1_3(
     A030001    varchar(32)     comment '员工ID'
    ,A030002    varchar(24)     comment '机构ID'
    ,A030003    varchar(200)    comment '姓名'
    ,A030004    varchar(3)      comment '国家地区'
    ,A030005    varchar(4)      comment '证件类型'
    ,A030006    varchar(100)    comment '证件号码'
    ,A030007    varchar(128)    comment '手机号码'
    ,A030008    varchar(128)    comment '办公电话'
    ,A030009    varchar(10)     comment '入职日期'
    ,A030010    string          comment '所属部门'
    ,A030011    varchar(200)    comment '职务'
    ,A030012    varchar(1)      comment '高管标识'
    ,A030013    varchar(10)     comment '批复日期'
    ,A030014    varchar(10)     comment '任职日期'
    ,A030015    varchar(2)      comment '员工类型'
    ,A030016    varchar(100)    comment '岗位编号'
    ,A030017    varchar(100)    comment '岗位名称'
    ,A030018    varchar(5)      comment '岗位标识'
    ,A030019    varchar(10)     comment '上岗日期'
    ,A030020    varchar(10)     comment '最近一次轮岗日期'
    ,A030021    varchar(10)     comment '最近一次强制休假日期'
    ,A030022    varchar(2)      comment '员工状态'
    ,A030023    varchar(24)     comment '柜员号'
    ,A030024    varchar(2)      comment '柜员类型'
    ,A030025    varchar(2)      comment '柜员权限级别'
    ,A030026    varchar(2)      comment '柜员状态'
    ,A030027    string          comment '备注'
    ,A030028    varchar(10)     comment '采集日期'
)
comment '1.3员工'

;

-- 1.4岗位信息
drop table if exists T_1_4;
create table T_1_4(
     A040001    varchar(24)     comment '机构ID'
    ,A040002    varchar(100)    comment '岗位编号'
    ,A040003    string          comment '岗位种类'
    ,A040004    varchar(100)    comment '岗位名称'
    ,A040005    string          comment '岗位说明'
    ,A040006    string          comment '岗位状态'
    ,A040007    string          comment '备注'
    ,A040008    varchar(10)     comment '采集日期'
)
comment '1.4岗位信息'

;

-- 1.5自助机具
drop table if exists T_1_5;
create table T_1_5(
     A050001    varchar(32)     comment '机具ID'
    ,A050002    varchar(24)     comment '机构ID'
    ,A050003    varchar(2)      comment '机具类型'
    ,A050004    string          comment '设备供应商'
    ,A050005    string          comment '设备维护商'
    ,A050006    string          comment '机具型号'
    ,A050007    string          comment '设备地址'
    ,A050008    varchar(32)     comment '虚拟柜员ID'
    ,A050009    varchar(10)     comment '设备启用日期'
    ,A050010    varchar(10)     comment '设备停用日期'
    ,A050011    varchar(2)      comment '运营状态'
    ,A050012    varchar(10)     comment '采集日期'
)
comment '1.5自助机具'

;

-- 1.6股东及关联方信息
drop table if exists T_1_6;
create table T_1_6(
     A060001    varchar(60)     comment '股东或关联方ID'
    ,A060002    varchar(24)     comment '机构ID'
    ,A060003    varchar(200)    comment '股东或关联方名称'
    ,A060004    varchar(2)      comment '股东或关联方类型'
    ,A060005    varchar(4)      comment '股东或关联方证件类型'
    ,A060006    varchar(100)    comment '股东或关联方证件号码'
    ,A060007    varchar(5)      comment '股东或关联方行业类型'
    ,A060008    varchar(255)    comment '股东或关联方注册地址'
    ,A060009    varchar(2)       comment '关系类型'
    ,A060010    varchar(200)    comment '实际控制人名称'
    ,A060011    int             comment '参股商业银行的数量'
    ,A060012    int             comment '控股商业银行的数量'
    ,A060013    varchar(2)      comment '不良信息'
    ,A060014    varchar(1)      comment '是否限权'
    ,A060015    varchar(2)      comment '入股资金来源'
    ,A060016    string          comment '入股资金账号'
    ,A060017    varchar(2)      comment '股东或关联方状态'
    ,A060018    int             comment '股东持股数量'
    ,A060019    decimal(6,2)    comment '股东持股比例'
    ,A060020    varchar(10)     comment '入股日期'
    ,A060021    decimal(6,2)    comment '股东股权质押比例'
    ,A060022    varchar(1)       comment '是否驻派董监事'
    ,A060023    varchar(10)     comment '最近一次变动日期'
    ,A060024    varchar(10)     comment '采集日期'
)
comment '1.6股东及关联方信息'

;

-- 2.客户类数据
-- 2.1单一法人基本情况
drop table if exists T_2_1;
create table T_2_1(
     B010001    varchar(60)     comment '客户ID'
    ,B010002    varchar(24)     comment '机构ID'
    ,B010003    varchar(200)    comment '对公客户名称'
    ,B010004    varchar(18)     comment '统一社会信用代码'
    ,B010005    varchar(10)     comment '组织机构登记/年检/更新日期'
    ,B010006    varchar(100)    comment '登记注册代码'
    ,B010007    varchar(10)     comment '登记注册/年检/更新日期'
    ,B010008    varchar(20)     comment '全球法人识别编码'
    ,B010009    string          comment '是否为POS机特约商户'
    ,B010010    string          comment '终端号'
    ,B010011    varchar(4)      comment '商户类别码'
    ,B010012    string          comment '商户类别码名称'
    ,B010013    string          comment '清算卡号或账号'
    ,B010014    varchar(2)      comment '清算账号类型'
    ,B010015    string          comment '清算账户名称'
    ,B010016    string          comment '清算账号开户行名称'
    ,B010017    varchar(10)     comment '商户起效日期'
    ,B010018    varchar(10)     comment '商户失效日期'
    ,B010019    decimal(20,2)   comment '注册资本'
    ,B010020    varchar(3)      comment '注册资本币种'
    ,B010021    decimal(20,2)   comment '实收资本'
    ,B010022    varchar(3)      comment '实收资本币种'
    ,B010023    varchar(10)     comment '成立日期'
    ,B010024    string          comment '经营范围'
    ,B010025    varchar(5)      comment '行业类型'
    ,B010026    varchar(2)      comment '对公客户类型'
    ,B010027    varchar(2)      comment '控股类型'
    ,B010028    varchar(3)      comment '注册地国家地区'
    ,B010029    varchar(255)    comment '注册地址'
    ,B010030    varchar(6)      comment '注册地行政区划'
    ,B010031    varchar(128)    comment '电话号码'
    ,B010032    varchar(200)    comment '法定代表人姓名'
    ,B010033    varchar(4)      comment '法定代表人证件类型'
    ,B010034    varchar(100)    comment '法定代表人证件号码'
    ,B010035    varchar(200)    comment '财务人员姓名'
    ,B010036    varchar(4)      comment '财务人员证件类型'
    ,B010037    varchar(100)    comment '财务人员证件号码'
    ,B010038    string          comment '基本存款账号'
    ,B010039    varchar(12)     comment '基本存款账户开户行行号'
    ,B010040    string          comment '基本存款账户开户行名称'
    ,B010041    int             comment '员工人数'
    ,B010042    varchar(1)      comment '上市企业标识'
    ,B010043    varchar(1)      comment '新型农业经营主体标识'
    ,B010044    string          comment '信用评级结果'
    ,B010045    string          comment '信用评级机构'
    ,B010046    string          comment '内部评级结果'
    ,B010047    string          comment '环境和社会风险分类'
    ,B010048    varchar(7)      comment '首次建立信贷关系年月'
    ,B010049    varchar(30)     comment '风险预警信号'
    ,B010050    varchar(30)     comment '关注事件代码'
    ,B010051    varchar(1)      comment '收单商户标识'
    ,B010052    string          comment '商户编号'
    ,B010053    varchar(1)      comment '关停企业标识'
    ,B010054    varchar(1)      comment '高新技术企业标识'
    ,B010055    varchar(1)      comment '科技型企业标识'
    ,B010056    varchar(1)      comment '科创企业标识'
    ,B010057    string          comment '母公司名称'
    ,B010058    decimal(10,6)   comment '违约概率'
    ,B010059    string          comment '商户状态'
    ,B010060    varchar(10)     comment '采集日期'
)
comment '2.1单一法人基本情况'

;

-- 2.2集团基本情况
drop table if exists T_2_2;
create table T_2_2(
     B020001    varchar(60)     comment '集团ID'
    ,B020002    varchar(24)     comment '机构ID'
    ,B020003    varchar(18)     comment '母公司统一社会信用代码'
    ,B020004    varchar(100)    comment '工商注册编号'
    ,B020005    string          comment '母公司名称'
    ,B020006    varchar(2)      comment '授信类型'
    ,B020007    string          comment '集团名称'
    ,B020008    int             comment '集团成员数'
    ,B020009    varchar(255)    comment '注册地址'
    ,B020010    varchar(3)      comment '注册地国家地区'
    ,B020011    varchar(6)      comment '注册地行政区划'
    ,B020012    varchar(10)     comment '更新注册信息日期'
    ,B020013    varchar(255)    comment '国内办公地址'
    ,B020014    varchar(6)      comment '国内办公地址行政区划'
    ,B020015    varchar(10)     comment '更新办公地址日期'
    ,B020016    varchar(30)     comment '风险预警信号'
    ,B020017    varchar(30)     comment '关注事件代码'
    ,B020018    string          comment '信用评级结果'
    ,B020019    varchar(10)     comment '采集日期'
)
comment '2.2集团基本情况'

;

-- 2.3同业客户基本情况
drop table if exists T_2_3;
create table T_2_3(
     B030001    varchar(60)     comment '同业ID'
    ,B030002    varchar(24)     comment '机构ID'
    ,B030003    string          comment '客户名称'
    ,B030004    varchar(2)      comment '机构类型'
    ,B030005    varchar(15)     comment '金融许可证号'
    ,B030006    varchar(11)     comment 'SWIFT编码'
    ,B030007    varchar(9)      comment '组织机构代码'
    ,B030008    string          comment '经营范围'
    ,B030009    varchar(10)     comment '成立日期'
    ,B030010    varchar(255)    comment '注册地址'
    ,B030011    varchar(3)      comment '注册地国家地区'
    ,B030012    varchar(6)      comment '注册地行政区划'
    ,B030013    varchar(200)    comment '法定代表人姓名'
    ,B030014    varchar(4)      comment '法定代表人证件类型'
    ,B030015    varchar(100)    comment '法定代表人证件号码'
    ,B030016    varchar(200)    comment '财务人员姓名'
    ,B030017    varchar(4)      comment '财务人员证件类型'
    ,B030018    varchar(100)    comment '财务人员证件号码'
    ,B030019    string          comment '基本存款账号'
    ,B030020    varchar(12)     comment '基本存款账户开户行行号'
    ,B030021    string          comment '基本存款账户开户行名称'
    ,B030022    decimal(20,2)   comment '注册资本'
    ,B030023    varchar(3)      comment '注册资本币种'
    ,B030024    decimal(20,2)   comment '实收资本'
    ,B030025    varchar(3)      comment '实收资本币种'
    ,B030026    varchar(1)      comment '上市企业标识'
    ,B030027    int             comment '员工人数'
    ,B030028    varchar(200)    comment '负责人姓名'
    ,B030029    varchar(128)    comment '机构联系电话'
    ,B030030    string          comment '信用评级结果'
    ,B030031    string          comment '信用评级机构'
    ,B030032    string          comment '内部评级结果'
    ,B030033    varchar(10)     comment '首次授信日期'
    ,B030034    varchar(30)     comment '风险预警信号'
    ,B030035    varchar(30)     comment '关注事件代码'
    ,B030036    varchar(10)     comment '采集日期'
)
comment '2.3同业客户基本情况'

;

-- 2.4个体工商户及小微企业主基本情况
drop table if exists T_2_4;
create table T_2_4(
     B040001    varchar(60)     comment '客户ID'
    ,B040002    varchar(24)     comment '机构ID'
    ,B040003    varchar(200)    comment '经营者姓名'
    ,B040004    varchar(4)      comment '经营者证件类型'
    ,B040005    varchar(100)    comment '经营者证件号码'
    ,B040006    int             comment '经营者从业年限'
    ,B040007    varchar(1)      comment '是否为POS机特约商户'
    ,B040008    string          comment '终端号'
    ,B040009    varchar(4)      comment '商户类别码'
    ,B040010    string          comment '商户类别码名称'
    ,B040011    string          comment '清算卡号或账号'
    ,B040012    varchar(2)      comment '清算账号类型'
    ,B040013    string          comment '清算账户名称'
    ,B040014    string          comment '清算账号开户行名称'
    ,B040015    varchar(10)     comment '商户起效日期'
    ,B040016    varchar(10)     comment '商户失效日期'
    ,B040017    varchar(1)      comment '收单商户标识'
    ,B040018    string          comment '商户编号'
    ,B040019    string          comment '经营范围'
    ,B040020    varchar(5)      comment '行业类型'
    ,B040021    varchar(2)      comment '商户类型'
    ,B040022    varchar(255)    comment '经营地址'
    ,B040023    varchar(6)      comment '经营地所在行政区划'
    ,B040024    decimal(20,2)   comment '资产总额'
    ,B040025    decimal(20,2)   comment '负债总额'
    ,B040026    decimal(20,2)   comment '税前利润'
    ,B040027    decimal(20,2)   comment '主营业务收入'
    ,B040028    varchar(10)     comment '财务报表日期'
    ,B040029    string          comment '信用评级结果'
    ,B040030    varchar(7)      comment '首次建立信贷关系年月'
    ,B040031    varchar(10)     comment '采集日期'
)
comment '2.4个体工商户及小微企业主基本情况'

;

-- 2.5个人客户基本情况
drop table if exists T_2_5;
create table T_2_5(
     B050001    varchar(60)     comment '客户ID'
    ,B050002    varchar(24)     comment '机构ID'
    ,B050003    varchar(200)    comment '个人客户名称'
    ,B050004    varchar(2)      comment '客户类型'
    ,B050005    varchar(100)    comment '客户身份证'
    ,B050006    varchar(100)    comment '客户护照号'
    ,B050007    varchar(4)      comment '客户其他证件类型'
    ,B050008    varchar(100)    comment '客户其他证件号码'
    ,B050009    string          comment '民族'
    ,B050010    varchar(2)      comment '性别'
    ,B050011    varchar(2)      comment '学历'
    ,B050012    varchar(10)     comment '出生日期'
    ,B050013    varchar(1)      comment '已婚标识'
    ,B050014    varchar(128)    comment '电话1'
    ,B050015    varchar(128)    comment '电话2'
    ,B050016    string          comment '工作单位名称'
    ,B050017    varchar(128)    comment '工作单位电话'
    ,B050018    varchar(255)    comment '工作单位地址'
    ,B050019    varchar(2)      comment '单位性质'
    ,B050020    varchar(5)      comment '职业'
    ,B050021    varchar(200)    comment '职务'
    ,B050022    decimal(20,2)   comment '个人年收入'
    ,B050023    decimal(20,2)   comment '家庭收入'
    ,B050024    varchar(255)    comment '通讯地址'
    ,B050025    varchar(6)      comment '通讯地址行政区划'
    ,B050026    varchar(1)      comment '本行员工标识'
    ,B050027    varchar(7)      comment '首次建立信贷关系年月'
    ,B050028    varchar(1)      comment '上本行黑名单标识'
    ,B050029    varchar(10)     comment '上黑名单日期'
    ,B050030    string          comment '上黑名单原因'
    ,B050031    varchar(1)      comment '居民标识'
    ,B050032    varchar(3)      comment '国家地区'
    ,B050033    varchar(1)      comment '农户及新型农业经营主体标识'
    ,B050034    varchar(1)      comment '已脱贫人口标识'
    ,B050035    varchar(1)      comment '边缘易致贫人口标识'
    ,B050036    varchar(10)     comment '采集日期'
)
comment '2.5个人客户基本情况'

;

-- 2.6客户财务信息
drop table if exists T_2_6;
create table T_2_6(
     B060001     varchar(60)     comment '客户ID'
    ,B060002     varchar(24)     comment '机构ID'
    ,B060003     varchar(200)    comment '对公客户名称'
    ,B060004     varchar(10)     comment '财务报表日期'
    ,B060005     varchar(1)      comment '是否审计'
    ,B060006     string          comment '审计机构'
    ,B060007     varchar(2)      comment '报表口径'
    ,B060008     varchar(3)      comment '币种'
    ,B060009     decimal(20,2)   comment '资产总额'
    ,B060010     decimal(20,2)   comment '负债总额'
    ,B060011     decimal(20,2)   comment '所得税'
    ,B060012     decimal(20,2)   comment '净利润'
    ,B060013     decimal(20,2)   comment '主营业务收入'
    ,B060014     decimal(20,2)   comment '存货'
    ,B060015     decimal(20,2)   comment '现金流量净额'
    ,B060016     decimal(20,2)   comment '应收账款'
    ,B060017     decimal(20,2)   comment '其他应收款'
    ,B060018     decimal(20,2)   comment '流动资产合计'
    ,B060019     decimal(20,2)   comment '流动负债合计'
    ,B060020     varchar(2)      comment '报表周期'
    ,B060021     varchar(40)     comment '财务报表编号'
    ,B060022     varchar(10)     comment '采集日期'
)
comment '2.6客户财务信息'

;

-- 3.关系类数据
-- 3.1重要股东及主要关联企业
drop table if exists T_3_1;
create table T_3_1(
     C010001    varchar(64)     comment '关系ID'
    ,C010002    varchar(60)     comment '法人ID'
    ,C010003    varchar(24)     comment '机构ID'
    ,C010004    string          comment '公司客户名称'
    ,C010005    varchar(200)    comment '股东名称'
    ,C010006    varchar(1)      comment '实际控制人标识'
    ,C010007    varchar(4)      comment '股东证件类型'
    ,C010008    varchar(100)    comment '股东证件号码'
    ,C010009    varchar(100)    comment '登记注册代码'
    ,C010010    varchar(2)      comment '股东类别'
    ,C010011    varchar(3)      comment '股东国家地区'
    ,C010012    decimal(6,2)    comment '企业股东持股比例'
    ,C010013    varchar(10)     comment '更新信息日期'
    ,C010014    varchar(10)     comment '股东结构对应日期'
    ,C010015    varchar(2)      comment '关联类型'
    ,C010016    varchar(2)      comment '关系状态'
    ,C010017    varchar(10)     comment '采集日期'
)
comment '3.1重要股东及主要关联企业'

;

-- 3.2高管及重要关系人信息
drop table if exists T_3_2;
create table T_3_2(
     C020001    varchar(64)     comment '关系ID'
    ,C020002    varchar(60)     comment '法人ID'
    ,C020003    varchar(24)     comment '机构ID'
    ,C020004    varchar(200)    comment '关系人姓名'
    ,C020005    varchar(4)      comment '关系人证件类型'
    ,C020006    varchar(100)    comment '关系人证件号码'
    ,C020007    varchar(10)     comment '证件签发日期'
    ,C020008    varchar(10)     comment '证件到期日期'
    ,C020009    varchar(7)      comment '关系人类型'
    ,C020010    varchar(2)      comment '关系人类别'
    ,C020011    varchar(3)      comment '关系人国家地区'
    ,C020012    varchar(10)     comment '更新信息日期'
    ,C020013    varchar(2)      comment '关系状态'
    ,C020014    varchar(10)     comment '采集日期'
)
comment '3.2高管及重要关系人信息'

;

-- 3.3集团成员名单
drop table if exists T_3_3;
create table T_3_3(
     C030001    varchar(64)     comment '关系ID'
    ,C030002    varchar(60)     comment '成员ID'
    ,C030003    string          comment '成员企业名称'
    ,C030004    varchar(18)     comment '成员统一社会信用代码'
    ,C030005    varchar(2)      comment '成员类型'
    ,C030006    varchar(100)    comment '登记注册代码'
    ,C030007    varchar(60)     comment '集团ID'
    ,C030008    varchar(24)     comment '机构ID'
    ,C030009    varchar(2)      comment '关系状态'
    ,C030010    varchar(10)     comment '采集日期'
)
comment '3.3集团成员名单'

;

-- 3.4集团实际控制人
drop table if exists T_3_4;
create table T_3_4(
     C040001    varchar(64)     comment '关系ID'
    ,C040002    varchar(60)     comment '集团ID'
    ,C040003    varchar(24)     comment '机构ID'
    ,C040004    varchar(200)    comment '实际控制人名称'
    ,C040005    varchar(2)      comment '实际控制人类别'
    ,C040006    varchar(3)      comment '实际控制人国家地区'
    ,C040007    varchar(4)      comment '实际控制人证件类型'
    ,C040008    varchar(100)    comment '实际控制人证件号码'
    ,C040009    varchar(100)    comment '登记注册代码'
    ,C040010    varchar(2)      comment '关系状态'
    ,C040011    varchar(10)     comment '采集日期'
)
comment '3.4集团实际控制人'

;

-- 3.5关联集团信息
drop table if exists T_3_5;
create table T_3_5(
     C050001    varchar(64)     comment '关系ID'
    ,C050002    varchar(60)     comment '集团ID'
    ,C050003    varchar(24)     comment '机构ID'
    ,C050004    varchar(60)     comment '关联集团ID'
    ,C050005    varchar(5)      comment '关联关系类型'
    ,C050006    varchar(2)      comment '关系状态'
    ,C050007    varchar(10)     comment '采集日期'
)
comment '3.5关联集团信息'

;

-- 3.6共同债务人
drop table if exists T_3_6;
create table T_3_6(
     C060001    varchar(64)     comment '关系ID'
    ,C060002    varchar(24)     comment '机构ID'
    ,C060003    varchar(200)    comment '共同债务人名称'
    ,C060004    varchar(4)      comment '共同债务人证件类型'
    ,C060005    varchar(100)    comment '共同债务人证件号码'
    ,C060006    varchar(60)     comment '借款人ID'
    ,C060007    varchar(60)     comment '借据ID'
    ,C060008    varchar(2)      comment '关系状态'
    ,C060009    varchar(10)     comment '采集日期'

)
comment '3.6共同债务人'

;

-- 3.7个人客户关系人
drop table if exists T_3_7;
create table T_3_7(
     C070001    varchar(64)     comment '关系ID'
    ,C070002    varchar(24)     comment '机构ID'
    ,C070003    varchar(60)     comment '个人ID'
    ,C070004    varchar(2)      comment '社会关系'
    ,C070005    varchar(60)     comment '关系人ID'
    ,C070006    varchar(200)    comment '关系人姓名'
    ,C070007    varchar(4)      comment '关系人证件类型'
    ,C070008    varchar(100)    comment '关系人证件号码'
    ,C070009    varchar(10)     comment '建立关系日期'
    ,C070010    varchar(10)     comment '解除关系日期'
    ,C070011    varchar(10)     comment '采集日期'
)
comment '3.7个人客户关系人'

;

-- 3.8交易与单据对应关系
drop table if exists T_3_8;
create table T_3_8(
     C080001    varchar(100)    comment '交易ID'
    ,C080002    varchar(60)     comment '协议ID'
    ,C080003    varchar(60)     comment '单据ID'
    ,C080004    varchar(24)     comment '交易机构ID'
    ,C080005    varchar(2)      comment '对应关系'
    ,C080006    string          comment '备注'
    ,C080007    varchar(10)     comment '采集日期'
)
comment '3.8交易与单据对应关系'

;

-- 4财务类数据
-- 4.1总账会计全科目
drop table if exists T_4_1;
create table T_4_1(
     D010001    varchar(24)     comment '机构ID'
    ,D010002    varchar(32)     comment '科目ID'
    ,D010003    decimal(20,2)   comment '期初借方余额'
    ,D010004    decimal(20,2)   comment '期初贷方余额'
    ,D010005    decimal(20,2)   comment '本期借方发生额'
    ,D010006    decimal(20,2)   comment '本期贷方发生额'
    ,D010007    decimal(20,2)   comment '期末借方余额'
    ,D010008    decimal(20,2)   comment '期末贷方余额'
    ,D010009    varchar(3)      comment '币种'
    ,D010010    varchar(10)     comment '会计日期'
    ,D010011    varchar(2)      comment '报表周期'
    ,D010012    varchar(10)     comment '采集日期'
)
comment '4.1总账会计全科目'

;

-- 4.2科目信息
drop table if exists T_4_2;
create table T_4_2(
     D020001    varchar(32)     comment '科目ID'
    ,D020002    varchar(24)     comment '机构ID'
    ,D020003    string          comment '科目名称'
    ,D020004    varchar(2)      comment '科目级次'
    ,D020005    varchar(2)      comment '科目类型'
    ,D020006    varchar(2)      comment '借贷标识'
    ,D020007    varchar(2)      comment '归属业务子类'
    ,D020008    varchar(32)     comment '上级科目ID'
    ,D020009    varchar(1)      comment '分户账标识'
    ,D020010    string          comment '备注'
    ,D020011    varchar(10)     comment '采集日期'
)
comment '4.2科目信息'

;

-- 4.3分户账信息
drop table if exists T_4_3;
create table T_4_3(
     D030001    varchar(24)     comment '机构ID'
    ,D030002    string          comment '分户账号'
    ,D030003    varchar(60)     comment '客户ID'
    ,D030004    string          comment '分户账名称'
    ,D030005    varchar(2)      comment '分户账类型'
    ,D030006    varchar(1)      comment '计息标识'
    ,D030007    varchar(2)      comment '计息方式'
    ,D030008    varchar(32)     comment '科目ID'
    ,D030009    varchar(3)      comment '币种'
    ,D030010    varchar(2)      comment '借贷标识'
    ,D030011    varchar(10)     comment '开户日期'
    ,D030012    varchar(10)     comment '销户日期'
    ,D030013    varchar(2)      comment '账户状态'
    ,D030014    string          comment '备注'
    ,D030015    varchar(10)     comment '采集日期'
)
comment '4.3分户账信息'

;

-- 4.4分户账变动情况
drop table if exists T_4_4;
create table T_4_4(
     D040001    string          comment '分户账号'
    ,D040002    varchar(24)     comment '机构ID'
    ,D040003    varchar(10)     comment '会计日期'
    ,D040004    varchar(3)      comment '币种'
    ,D040005    decimal(20,2)   comment '期初借方余额'
    ,D040006    decimal(20,2)   comment '期初贷方余额'
    ,D040007    decimal(20,2)   comment '本期借方发生额'
    ,D040008    decimal(20,2)   comment '本期贷方发生额'
    ,D040009    decimal(20,2)   comment '期末借方余额'
    ,D040010    decimal(20,2)   comment '期末贷方余额'
    ,D040011    decimal(20,2)   comment '应收利息'
    ,D040012    decimal(20,2)   comment '应付利息'
    ,D040013    varchar(10)     comment '采集日期'
)
comment '4.4分户账变动情况'

;

-- 5.产品类数据    

-- 5.1产品业务基本信息
drop table if exists T_5_1;
create table T_5_1(
     E010001    varchar(32)     comment '产品ID'
    ,E010002    varchar(24)     comment '机构ID'
    ,E010003    string          comment '产品名称'
    ,E010004    string          comment '产品编号'
    ,E010005    varchar(2)      comment '科目类型'
    ,E010006    varchar(2)      comment '归属业务子类'
    ,E010007    varchar(4)      comment '产品类别'
    ,E010008    varchar(2)      comment '自营标识'
    ,E010009    varchar(128)    comment '产品币种'
    ,E010010    int             comment '产品期限'
    ,E010011    varchar(10)     comment '产品成立日期'
    ,E010012    varchar(10)     comment '产品到期日期'
    ,E010013    int             comment '产品期次'
    ,E010014    varchar(2)      comment '利率类型'
    ,E010015    varchar(2)      comment '产品状态代码'
    ,E010016    string          comment '备注'
    ,E010017    varchar(10)     comment '采集日期'
)
comment '5.1产品业务基本信息'

;

-- 5.2贷款产品业务
drop table if exists T_5_2;
create table T_5_2(
     E020001    varchar(32)     comment '产品ID'
    ,E020002    varchar(24)     comment '机构ID'
    ,E020003    string          comment '产品名称'
    ,E020004    string          comment '产品编号'
    ,E020005    varchar(10)     comment '贷款新规种类'
    ,E020006    varchar(2)      comment '境内外贷款标识'
    ,E020007    varchar(1)      comment '并购贷款标识'
    ,E020008    varchar(1)      comment '住房抵押贷款标识'
    ,E020009    varchar(1)      comment '保障性安居工程贷款标识'
    ,E020010    varchar(1)      comment '互联网贷款标识'
    ,E020011    varchar(2)      comment '贷款对象类型代码'
    ,E020012    varchar(2)      comment '信贷业务种类'
    ,E020013    string          comment '备注'
    ,E020014    varchar(10)     comment '采集日期'
)
comment '5.2贷款产品业务'

;

-- 5.3债券产品业务
drop table if exists T_5_3;
create table T_5_3(
     E030001    varchar(32)     comment '产品ID'
    ,E030002    varchar(24)     comment '机构ID'
    ,E030003    string          comment '产品名称'
    ,E030004    string          comment '产品编号'
    ,E030005    varchar(2)      comment '债券产品业务类型'
    ,E030006    varchar(2)      comment '债券类型代码'
    ,E030007    varchar(2)      comment '债券子类型代码'
    ,E030008    decimal(20,2)   comment '票面金额'
    ,E030009    int             comment '债券期次'
    ,E030010    decimal(20,2)   comment '发行规模'
    ,E030011    varchar(18)     comment '债券发行人统一社会信用代码'
    ,E030012    varchar(200)    comment '债券发行人名称'
    ,E030013    string          comment '定期付息账号'
    ,E030014    varchar(1)      comment '可回购标识'
    ,E030015    varchar(1)      comment '可提前偿还标识'
    ,E030016    decimal(20,2)   comment '发行价格'
    ,E030017    decimal(20,2)   comment '赎回价格'
    ,E030018    varchar(3)      comment '发行国家地区'
    ,E030019    varchar(3)      comment '担保机构国家地区'
    ,E030020    varchar(2)      comment '发行机构类型'
    ,E030021    varchar(2)      comment '担保机构类型'
    ,E030022    varchar(2)      comment '发行方式'
    ,E030023    string          comment '发行人所在省'
    ,E030024    varchar(2)      comment '发行资金用途'
    ,E030025    decimal(10,6)   comment '资产风险权重'
    ,E030026    varchar(2)      comment '资产等级'
    ,E030027    varchar(2)      comment '主权风险权重'
    ,E030028    decimal(10,6)   comment '基准国债收益率'
    ,E030029    varchar(12)     comment '交易方式代码'
    ,E030030    varchar(10)     comment '发行日期'
    ,E030031    varchar(10)     comment '到期兑付日期'
    ,E030032    string          comment '备注'
    ,E030033    varchar(10)     comment '采集日期'
)
comment '5.3债券产品业务'

;

-- 5.4理财产品业务
drop table if exists T_5_4;
create table T_5_4(
     E040001    varchar(32)     comment '产品ID'
    ,E040002    varchar(24)     comment '机构ID'
    ,E040003    string          comment '产品编号'
    ,E040004    string          comment '理财产品代码'
    ,E040005    string          comment '产品名称'
    ,E040006    varchar(6)      comment '发行机构代码'
    ,E040007    string          comment '发行机构名称'
    ,E040008    varchar(2)      comment '发行机构类型'
    ,E040009    varchar(2)      comment '产品特殊属性'
    ,E040010    int             comment '产品期次'
    ,E040011    varchar(2)      comment '产品风险等级'
    ,E040012    varchar(2)      comment '目标客户类型'
    ,E040013    varchar(2)      comment '资金投向地区'
    ,E040014    varchar(2)      comment '产品运作模式'
    ,E040015    varchar(2)      comment '产品资产配置方式'
    ,E040016    varchar(2)      comment '产品管理模式'
    ,E040017    string          comment '实际管理人'
    ,E040018    varchar(2)      comment '管理方式'
    ,E040019    varchar(32)     comment '产品审批员工ID'
    ,E040020    varchar(100)    comment '投资经理身份证号'
    ,E040021    varchar(200)    comment '投资经理姓名'
    ,E040022    varchar(2)      comment '产品定价方式'
    ,E040023    varchar(2)      comment '结构性产品挂钩标的'
    ,E040024    varchar(2)      comment '合作模式'
    ,E040025    string          comment '合作机构名称'
    ,E040026    string          comment '投资资产种类及比例'
    ,E040027    varchar(128)    comment '募集币种'
    ,E040028    varchar(128)    comment '兑付本金币种'
    ,E040029    varchar(128)    comment '兑付收益币种'
    ,E040030    varchar(10)     comment '投资本金到账日'
    ,E040031    varchar(10)     comment '投资收益到账日'
    ,E040032    varchar(1)      comment '发行机构提前终止权标识'
    ,E040033    varchar(1)      comment '客户赎回权标识'
    ,E040034    varchar(1)      comment '产品增信标识'
    ,E040035    varchar(2)      comment '产品增信形式'
    ,E040036    varchar(1)      comment '结构化（分级）产品标识'
    ,E040037    varchar(2)      comment '层级类别'
    ,E040038    string          comment '收益率分档说明'
    ,E040039    varchar(2)      comment '开放模式'
    ,E040040    varchar(2)      comment '规律开放周期'
    ,E040041    int             comment '其他规律开放周期'
    ,E040042    varchar(1)      comment '节假日开放标识'
    ,E040043    int             comment '平均开放次数'
    ,E040044    varchar(2)      comment '开放期业务类型'
    ,E040045    varchar(10)     comment '募集起始日期'
    ,E040046    varchar(10)     comment '募集结束日期'
    ,E040047    varchar(10)     comment '产品实际终止日期'
    ,E040048    varchar(10)     comment '报告登记日期'
    ,E040049    varchar(10)     comment '终止登记日期'
    ,E040050    varchar(10)     comment '发行登记日期'
    ,E040051    varchar(10)     comment '募集登记日期'
    ,E040052    varchar(2)      comment '理财产品状态'
    ,E040053    varchar(128)    comment '产品销售区域'
    ,E040054    varchar(1)      comment '其他机构代销标识'
    ,E040055    string          comment '境内托管机构代码'
    ,E040056    string          comment '境内托管机构名称'
    ,E040057    varchar(128)    comment '境外托管机构国家地区'
    ,E040058    string          comment '境外托管机构名称'
    ,E040059    string          comment '理财产品资金托管账号'
    ,E040060    string          comment '理财产品资金托管账户名称'
    ,E040061    varchar(2)      comment '新老产品标识'
    ,E040062    varchar(2)      comment '产品募集方式'
    ,E040063    varchar(2)      comment '产品投资性质'
    ,E040064    varchar(1)      comment '设置最短持有期限标识'
    ,E040065    int             comment '最短持有期限（天）'
    ,E040066    varchar(1)      comment '最短持有期后自由赎回标识'
    ,E040067    varchar(1)      comment '现金管理类理财产品标识'
    ,E040068    decimal(10,6)   comment '业绩比较基准上限'
    ,E040069    decimal(10,6)   comment '业绩比较基准下限'
    ,E040070    string          comment '业绩比较基准说明'
    ,E040071    decimal(10,6)   comment '投资管理费率'
    ,E040072    varchar(1)      comment '金融同业专属标识'
    ,E040073    decimal(10,6)   comment '托管费率'
    ,E040074    decimal(20,2)   comment '计划募集金额'
    ,E040075    decimal(20,2)   comment '实际募集金额'
    ,E040076    decimal(20,2)   comment '银行实际实现收入'
    ,E040077    decimal(20,2)   comment '兑付客户收益'
    ,E040078    decimal(20,2)   comment '兑付客户总金额'
    ,E040079    decimal(10,6)   comment '客户实际年化收益率'
    ,E040080    decimal(10,6)   comment '产品实际年化收益率'
    ,E040081    string          comment '备注'
    ,E040082    varchar(10)     comment '采集日期'
)
comment '5.4理财产品业务'

;

-- 5.5代销保险产品业务
drop table if exists T_5_5;
create table T_5_5(
     E050001    varchar(32) comment '产品ID'
    ,E050002    varchar(24) comment '机构ID'
    ,E050003    string      comment '产品名称'
    ,E050004    string      comment '产品编号'
    ,E050005    string      comment '保险公司名称'
    ,E050006    varchar(4)  comment '险种子类型代码'
    ,E050007    string      comment '附加险产品编号'
    ,E050008    string      comment '附加险名称'
    ,E050009    string      comment '备注'
    ,E050010    varchar(10) comment '采集日期'
)
comment '5.5代销保险产品业务'

;

-- 5.6卡产品
drop table if exists T_5_6;
create table T_5_6(
     E060001    varchar(32)     comment '产品ID'
    ,E060002    varchar(24)     comment '机构ID'
    ,E060003    string          comment '产品名称'
    ,E060004    string          comment '产品编号'
    ,E060005    varchar(2)      comment '卡组织代码'
    ,E060006    varchar(2)      comment '卡类型'
    ,E060007    varchar(2)      comment '卡介质类型代码'
    ,E060008    varchar(1)      comment '允许取现标识'
    ,E060009    varchar(1)      comment '允许转出标识'
    ,E060010    varchar(1)      comment '收取费用标识'
    ,E060011    varchar(1)      comment '政策功能标识'
    ,E060012    varchar(1)      comment '虚拟卡标识'
    ,E060013    varchar(1)      comment '联名卡标识'
    ,E060014    string          comment '联名单位'
    ,E060015    varchar(128)    comment '联名单位代码'
    ,E060016    string          comment '备注'
    ,E060017    varchar(10)     comment '采集日期'
)
comment '5.6卡产品'

;



-- 6.协议类数据    
-- 6.1存款协议
drop table if exists T_6_1;
create table T_6_1(
     F010001     varchar(60)     comment '协议ID'
    ,F010002     varchar(24)     comment '机构ID'
    ,F010003     varchar(60)     comment '客户ID'
    ,F010004     varchar(2)      comment '客户类型'
    ,F010005     varchar(32)     comment '科目ID'
    ,F010006     varchar(32)     comment '产品ID'
    ,F010007     string          comment '存款账号'
    ,F010008     varchar(2)      comment '存款账户类型'
    ,F010009     varchar(2)      comment '提前支取标识'
    ,F010010     decimal(20,2)   comment '提前支取罚息'
    ,F010011     varchar(1)      comment '业务关系标识'
    ,F010012     varchar(1)      comment '行为性期权标识'
    ,F010013     varchar(2)      comment '交易介质'
    ,F010014     string          comment '交易介质号'
    ,F010015     varchar(60)     comment '交易介质ID'
    ,F010016     varchar(1)      comment '保证金账户标识'
    ,F010017     decimal(10,6)   comment '利率'
    ,F010018     varchar(2)      comment '利率定价基础'
    ,F010019     varchar(10)     comment '开户日期'
    ,F010020     decimal(20,2)   comment '开户金额'
    ,F010021     varchar(10)     comment '到期日期'
    ,F010022     varchar(3)      comment '协议币种'
    ,F010023     varchar(2)      comment '钞汇类别'
    ,F010024     varchar(10)     comment '销户日期'
    ,F010025     varchar(2)      comment '账户状态'
    ,F010026     varchar(2)      comment '账户资金控制情况'
    ,F010027     varchar(2)      comment '协议状态'
    ,F010028     varchar(32)     comment '经办员工ID'
    ,F010029     varchar(32)     comment '审查员工ID'
    ,F010030     varchar(32)     comment '审批员工ID'
    ,F010031     varchar(32)     comment '管户员工ID'
    ,F010032     string          comment '备注'
    ,F010033     varchar(10)     comment '启用日期'
    ,F010034     varchar(32)     comment '启用柜员ID'
    ,F010035     varchar(10)     comment '采集日期'
)
comment '6.1存款协议'

;

-- 6.2贷款协议
drop table if exists T_6_2;
create table T_6_2(
 F020001     varchar(60)     comment '协议ID'
,F020002     varchar(24)     comment '机构ID'
,F020003     varchar(60)     comment '客户ID'
,F020004     varchar(1)      comment '首贷户标识'
,F020005     string          comment '合同名称'
,F020006     varchar(32)     comment '产品ID'
,F020007     varchar(3)      comment '协议币种'
,F020008     decimal(16,2)   comment '贷款金额'
,F020009     varchar(2)      comment '担保方式'
,F020010     string          comment '贷款用途'
,F020011     varchar(5)      comment '行业类型（按客户所属行业划分）'
,F020012     varchar(5)      comment '行业类型（按贷款投向划分）'
,F020013     varchar(2)      comment '个人非经营性贷款所属类别'
,F020014     varchar(1)      comment '个人经营性贷款标识'
,F020015     varchar(1)      comment '贷款主体为地方政府融资平台标识'
,F020016     varchar(1)      comment '含行为性期权条款标识'
,F020017     varchar(1)      comment '循环贷标识'
,F020018     varchar(2)      comment '受托支付类型'
,F020019     varchar(1)      comment '银税合作贷款标识'
,F020020     varchar(1)      comment '银团贷款标识'
,F020021     varchar(1)      comment '绿色贷款标识'
,F020022     varchar(1)      comment '科技贷款标识'
,F020023     varchar(1)      comment '涉农贷款标识'
,F020024     varchar(2)      comment '普惠型小微企业和其它组织贷款标识（大类）'
,F020025     varchar(2)      comment '普惠型小微企业和其它组织贷款标识（中类）'
,F020026     varchar(2)      comment '普惠型小微企业和其它组织贷款标识（小类）'
,F020027     varchar(2)      comment '普惠型涉农贷款标识（大类）'
,F020028     varchar(2)      comment '普惠型涉农贷款标识（中类）'
,F020029     varchar(2)      comment '普惠型涉农贷款标识（小类）'
,F020030     varchar(5)      comment '普惠型消费贷款标识（大类）'
,F020031     varchar(5)      comment '普惠型消费贷款标识（小类）'
,F020032     varchar(1)      comment '创业担保贷款标识'
,F020033     varchar(1)      comment '无还本续贷贷款标识'
,F020034     string          comment '助学贷款-学校名称'
,F020035     string          comment '助学贷款-学校地址'
,F020036     varchar(6)      comment '助学贷款-学校行政区划'
,F020037     varchar(24)     comment '助学贷款-学生证号'
,F020038     string          comment '助学贷款-贷款时家庭住址'
,F020039     varchar(6)      comment '助学贷款-贷款时家庭住址行政区划'
,F020040     varchar(2)      comment '助学贷款类型'
,F020041     varchar(1)      comment '具备提前还款权标识'
,F020042     varchar(3)      comment '重点产业标识'
,F020043     varchar(2)      comment '投贷联动业务标识'
,F020044     varchar(2)      comment '投贷联动业务——联动方式标识'
,F020045     varchar(2)      comment '投贷联动业务——企业成长阶段标识'
,F020046     varchar(2)      comment '投贷联动业务——企业上市标识'
,F020047     varchar(2)      comment '投贷联动业务——不良贷款处置方式标识'
,F020048     varchar(10)     comment '贷款协议起始日期'
,F020049     varchar(10)     comment '贷款协议到期日期'
,F020050     varchar(2)      comment '计息方式'
,F020051     varchar(2)      comment '贷款利率定价基础'
,F020052     decimal(10,6)   comment '贷款利率'
,F020053     decimal(10,6)   comment '利率浮动'
,F020054     decimal(10,6)   comment '罚息利率'
,F020055     string          comment '贷款投向地区'
,F020056     int             comment '债务重组次数'
,F020057     varchar(32)     comment '经办员工ID'
,F020058     varchar(32)     comment '管户员工ID'
,F020059     varchar(32)     comment '审查员工ID'
,F020060     varchar(32)     comment '审批员工ID'
,F020061     varchar(2)      comment '协议状态'
,F020062     string          comment '备注'
,F020063     varchar(10)     comment '采集日期'
)
comment '6.2贷款协议'

;

-- 6.3项目贷款协议
drop table if exists T_6_3;
create table T_6_3(
     F030001     varchar(24)     comment '机构ID'
    ,F030002     varchar(60)     comment '协议ID'
    ,F030003     varchar(2)      comment '项目类型'
    ,F030004     string          comment '项目名称'
    ,F030005     decimal(20,2)   comment '项目总投资'
    ,F030006     decimal(20,2)   comment '项目资本金'
    ,F030007     string          comment '批文文号'
    ,F030008     string          comment '立项批文'
    ,F030009     string          comment '土地使用证编号'
    ,F030010     varchar(10)     comment '土地使用证日期'
    ,F030011     string          comment '用地规划许可证编号'
    ,F030012     varchar(10)     comment '用地规划许可证日期'
    ,F030013     string          comment '施工许可证编号'
    ,F030014     varchar(10)     comment '施工许可证日期'
    ,F030015     string          comment '工程规划许可证编号'
    ,F030016     varchar(10)     comment '工程规划许可证日期'
    ,F030017     string          comment '其他许可证'
    ,F030018     string          comment '其他许可证编号'
    ,F030019     varchar(10)     comment '开工日期'
    ,F030020     string          comment '备注'
    ,F030021     varchar(10)     comment '采集日期'
)
comment '6.3项目贷款协议'

;

-- 6.4互联网贷款协议
drop table if exists T_6_4;
create table T_6_4(
     F040001     varchar(24)    comment '机构ID'
    ,F040002     varchar(60)    comment '协议ID'
    ,F040003     varchar(60)    comment '借据ID'
    ,F040004     varchar(100)   comment '合作协议ID'
    ,F040005     varchar(2)     comment '业务模式'
    ,F040006     decimal(16,2)  comment '合作方负有担保责任的金额'
    ,F040007     decimal(16,2)  comment '合作方出资发放贷款金额'
    ,F040008     decimal(16,2)  comment '本机构出资发放贷款金额'
    ,F040009     string         comment '客户数据授权书编号'
    ,F040010     varchar(10)    comment '授权生效日期'
    ,F040011     varchar(10)    comment '授权终止日期'
    ,F040012     string         comment '提供部分风险评价服务合作机构名称'
    ,F040013     string         comment '提供担保增信合作机构名称'
    ,F040014     varchar(2)     comment '协议状态'
    ,F040015     string         comment '备注'
    ,F040016     varchar(10)    comment '采集日期'
)
comment '6.4互联网贷款协议'

;

-- 6.5银团贷款协议
drop table if exists T_6_5;
create table T_6_5(
     F050001    varchar(60)     comment '协议ID'
    ,F050002    string          comment '牵头行行名'
    ,F050003    string          comment '牵头行行号'
    ,F050004    string          comment '参加行行名'
    ,F050005    string          comment '参加行行号'
    ,F050006    string          comment '代理行行名'
    ,F050007    string          comment '代理行行号'
    ,F050008    varchar(2)      comment '银团成员类型'
    ,F050009    decimal(16,2)   comment '银团贷款总金额'
    ,F050010    decimal(16,2)   comment '承担贷款金额'
    ,F050011    decimal(20,2)   comment '已发放银团贷款金额'
    ,F050012    decimal(20,2)   comment '已发放银团贷款余额'
    ,F050013    string          comment '备注'
    ,F050014    varchar(10)     comment '采集日期'
)
comment '6.5银团贷款协议'

;

-- 6.6受托支付信息
drop table if exists T_6_6;
create table T_6_6(
     F060001    varchar(60)     comment '协议ID'
    ,F060002    varchar(60)     comment '借据ID'
    ,F060003    decimal(16,2)   comment '受托支付金额'
    ,F060004    varchar(10)     comment '受托支付日期'
    ,F060005    string          comment '受托支付对象账号'
    ,F060006    string          comment '受托支付对象户名'
    ,F060007    string          comment '受托支付对象行号'
    ,F060008    string          comment '受托支付对象行名'
    ,F060009    string          comment '备注'
    ,F060010    varchar(10)     comment '采集日期'
)
comment '6.6受托支付信息'

;

-- 6.7贷款展期协议
drop table if exists T_6_7;
create table T_6_7(
     F070001    varchar(60)     comment '协议ID'
    ,F070002    varchar(60)     comment '借据ID'
    ,F070003    int             comment '展期次数'
    ,F070004    string          comment '被展期贷款的贷款协议'
    ,F070005    varchar(10)     comment '展期贷款的到期日期'
    ,F070006    decimal(16,2)   comment '展期贷款的贷款金额'
    ,F070007    string          comment '展期贷款的贷款用途'
    ,F070008    decimal(10,6)   comment '展期贷款的贷款利率'
    ,F070009    varchar(10)     comment '采集日期'
)
comment '6.7贷款展期协议'

;

-- 6.8担保协议
drop table if exists T_6_8;
create table T_6_8(
     F080001    varchar(60)     comment '协议ID'
    ,F080002    varchar(24)     comment '机构ID'
    ,F080003    varchar(60)     comment '被担保协议ID'
    ,F080004    varchar(2)      comment '担保类型'
    ,F080005    varchar(2)      comment '担保合同方向'
    ,F080006    varchar(2)      comment '被担保业务类型'
    ,F080007    varchar(2)      comment '担保合同类型'
    ,F080008    varchar(2)      comment '担保人类别'
    ,F080009    varchar(200)    comment '担保人名称'
    ,F080010    varchar(4)      comment '担保人证件类型'
    ,F080011    varchar(100)    comment '担保人证件号码'
    ,F080012    varchar(10)     comment '签约日期'
    ,F080013    varchar(10)     comment '生效日期'
    ,F080014    varchar(10)     comment '到期日期'
    ,F080015    decimal(20,2)   comment '协议金额'
    ,F080016    varchar(3)      comment '协议币种'
    ,F080017    varchar(3)      comment '担保人净资产币种'
    ,F080018    decimal(20,2)   comment '担保人净资产'
    ,F080019    varchar(2)      comment '协议状态'
    ,F080020    varchar(32)     comment '经办员工ID'
    ,F080021    varchar(32)     comment '审查员工ID'
    ,F080022    varchar(32)     comment '审批员工ID'
    ,F080023    varchar(1)      comment '或有负债标识'
    ,F080024    string          comment '备注'
    ,F080025    varchar(10)     comment '采集日期'
)
comment '6.8担保协议'

;

-- 6.9信用卡协议
drop table if exists T_6_9;
create table T_6_9(
     F090001    varchar(60)     comment '协议ID'
    ,F090002    varchar(24)     comment '机构ID'
    ,F090003    varchar(60)     comment '客户ID'
    ,F090004    varchar(32)     comment '产品ID'
    ,F090005    string          comment '发卡合作机构'
    ,F090006    varchar(18)     comment '发卡合作机构代码'
    ,F090007    string          comment '卡号'
    ,F090008    string          comment '发卡渠道'
    ,F090009    varchar(1)      comment '准贷记卡标识'
    ,F090010    varchar(1)      comment '个人卡标识'
    ,F090011    varchar(1)      comment '员工卡标识'
    ,F090012    varchar(40)     comment '主卡号'
    ,F090013    varchar(1)      comment '附属卡标识'
    ,F090014    varchar(1)      comment '年费标识'
    ,F090015    varchar(1)      comment '快捷支付标识'
    ,F090016    varchar(1)      comment '网络支付标识'
    ,F090017    varchar(2)      comment '主要担保方式'
    ,F090018    decimal(20,2)   comment '总授信额度上限'
    ,F090019    decimal(20,2)   comment '本币信用额度'
    ,F090020    decimal(20,2)   comment '外币信用额度'
    ,F090021    varchar(3)      comment '外币币种'
    ,F090022    decimal(20,2)   comment '本币现金支取额度'
    ,F090023    decimal(20,2)   comment '外币现金支取额度'
    ,F090024    varchar(10)     comment '受理日期'
    ,F090025    int             comment '交易账单日期'
    ,F090026    int             comment '最迟还款天数'
    ,F090027    varchar(10)     comment '开卡日期'
    ,F090028    varchar(32)     comment '开卡经办员工ID'
    ,F090029    varchar(2)      comment '卡状态'
    ,F090030    varchar(2)      comment '异常标识'
    ,F090031    varchar(2)      comment '限制措施'
    ,F090032    varchar(10)     comment '销卡日期'
    ,F090033    varchar(32)     comment '销卡经办员工ID'
    ,F090034    string          comment '卡片级别'
    ,F090035    string          comment '担保说明'
    ,F090036    varchar(10)     comment '采集日期'
)
comment '6.9信用卡协议'

;

-- 6.10贸易融资协议
drop table if exists T_6_10;
create table T_6_10(
     F100001    varchar(24)     comment '机构ID'
    ,F100002    varchar(60)     comment '协议ID'
    ,F100003    varchar(32)     comment '产品ID'
    ,F100004    varchar(3)      comment '协议币种'
    ,F100005    varchar(2)      comment '贸易融资品种'
    ,F100006    decimal(16,2)   comment '贸易融资金额'
    ,F100007    varchar(10)     comment '发放日期'
    ,F100008    varchar(10)     comment '到期日期'
    ,F100009    varchar(200)    comment '购货方名称'
    ,F100010    varchar(200)    comment '销货方名称'
    ,F100011    string          comment '贸易交易内容'
    ,F100012    string          comment '开证行名称'
    ,F100013    varchar(200)    comment '支付对象名称'
    ,F100014    varchar(3)      comment '手续费币种'
    ,F100015    decimal(16,2)   comment '手续费金额'
    ,F100016    string          comment '保证金账号'
    ,F100017    decimal(4,2)    comment '保证金比例'
    ,F100018    varchar(3)      comment '保证金币种'
    ,F100019    decimal(16,2)   comment '保证金金额'
    ,F100020    varchar(3)      comment '重点产业标识'
    ,F100021    varchar(32)     comment '经办员工ID'
    ,F100022    varchar(32)     comment '审查员工ID'
    ,F100023    varchar(32)     comment '审批员工ID'
    ,F100024    varchar(200)    comment '还款对象名称'
    ,F100025    string          comment '备注'
    ,F100026    varchar(10)     comment '采集日期'
)
comment '6.10贸易融资协议'

;

-- 6.11信用证协议
drop table if exists T_6_11;
create table T_6_11(
     F110001    varchar(60)     comment '协议ID'
    ,F110002    varchar(100)    comment '业务号码'
    ,F110003    varchar(24)     comment '机构ID'
    ,F110004    varchar(60)     comment '客户ID'
    ,F110005    varchar(32)     comment '产品ID'
    ,F110006    varchar(2)      comment '信用证种类'
    ,F110007    varchar(60)     comment '信用证ID'
    ,F110008    varchar(3)      comment '协议币种'
    ,F110009    decimal(16,2)   comment '开证金额'
    ,F110010    varchar(10)     comment '开证日期'
    ,F110011    varchar(10)     comment '到期日期'
    ,F110012    varchar(2)      comment '支付类型'
    ,F110013    int             comment '远期天数'
    ,F110014    decimal(10,6)   comment '垫款利率'
    ,F110015    string          comment '贸易合同编号'
    ,F110016    string          comment '货品名称'
    ,F110017    decimal(16,2)   comment '贸易合同金额'
    ,F110018    string          comment '合同贸易背景'
    ,F110019    varchar(3)      comment '申请人国家代码'
    ,F110020    varchar(200)    comment '受益人名称'
    ,F110021    varchar(3)      comment '受益人国家地区'
    ,F110022    string          comment '受益人开户行名称'
    ,F110023    varchar(3)      comment '重点产业标识'
    ,F110024    varchar(1)      comment '代开信用证标识'
    ,F110025    string          comment '代开信用证的申请行的行名'
    ,F110026    int             comment '支付期限'
    ,F110027    varchar(3)      comment '手续费币种'
    ,F110028    decimal(16,2)   comment '手续费金额'
    ,F110029    string          comment '保证金账号'
    ,F110030    varchar(3)      comment '保证金币种'
    ,F110031    decimal(16,2)   comment '保证金金额'
    ,F110032    decimal(4,2)    comment '保证金比例'
    ,F110033    varchar(32)     comment '经办员工ID'
    ,F110034    varchar(32)     comment '审查员工ID'
    ,F110035    varchar(32)     comment '审批员工ID'
    ,F110036    string          comment '备注'
    ,F110037    string          comment '受益人开户行账号'
    ,F110038    varchar(10)     comment '采集日期'
)
comment '6.11信用证协议'

;

-- 6.12保函协议
drop table if exists T_6_12;
create table T_6_12(
     F120001    varchar(60)     comment '协议ID'
    ,F120002    varchar(100)    comment '业务号码'
    ,F120003    varchar(24)     comment '机构ID'
    ,F120004    varchar(60)     comment '客户ID'
    ,F120005    varchar(2)      comment '保函类型'
    ,F120006    varchar(32)     comment '科目ID'
    ,F120007    string          comment '科目名称'
    ,F120008    varchar(1)      comment '融资性保函标识'
    ,F120009    decimal(16,2)   comment '保函金额'
    ,F120010    varchar(3)      comment '协议币种'
    ,F120011    string          comment '合同贸易背景'
    ,F120012    varchar(3)      comment '重点产业标识'
    ,F120013    varchar(10)     comment '生效日期'
    ,F120014    varchar(10)     comment '到期日期'
    ,F120015    string          comment '保证金账号'
    ,F120016    varchar(3)      comment '保证金币种'
    ,F120017    decimal(16,2)   comment '保证金金额'
    ,F120018    decimal(4,2)    comment '保证金比例'
    ,F120019    decimal(16,2)   comment '手续费金额'
    ,F120020    varchar(3)      comment '手续费币种'
    ,F120021    varchar(200)    comment '受益人名称'
    ,F120022    varchar(3)      comment '受益人国家地区'
    ,F120023    decimal(20,2)   comment '待支付金额'
    ,F120024    varchar(32)     comment '经办员工ID'
    ,F120025    varchar(32)     comment '审查员工ID'
    ,F120026    varchar(32)     comment '审批员工ID'
    ,F120027    string          comment '备注'
    ,F120028    varchar(10)     comment '采集日期'
)
comment '6.12保函协议'

;

-- 6.13票据协议
drop table if exists T_6_13;
create table T_6_13(
     F130001    varchar(60)     comment '协议ID'
    ,F130002    varchar(100)    comment '业务号码'
    ,F130003    varchar(24)     comment '机构ID'
    ,F130004    varchar(60)     comment '客户ID'
    ,F130005    string          comment '出票人名称'
    ,F130006    varchar(200)    comment '收款人名称'
    ,F130007    string          comment '承兑人名称'
    ,F130008    varchar(2)      comment '业务类型'
    ,F130009    varchar(32)     comment '科目ID'
    ,F130010    string          comment '科目名称'
    ,F130011    string          comment '收款人账号'
    ,F130012    string          comment '收款人开户行名称'
    ,F130013    string          comment '出票人账号'
    ,F130014    string          comment '出票人开户行名称'
    ,F130015    varchar(2)      comment '票据类型'
    ,F130016    varchar(60)     comment '票据号码'
    ,F130017    varchar(1)      comment '电票标识'
    ,F130018    varchar(3)      comment '重点产业标识'
    ,F130019    varchar(3)      comment '协议币种'
    ,F130020    decimal(16,2)   comment '票面金额'
    ,F130021    string          comment '保证金账号'
    ,F130022    varchar(3)      comment '保证金币种'
    ,F130023    decimal(16,2)   comment '保证金金额'
    ,F130024    decimal(4,2)    comment '保证金比例'
    ,F130025    varchar(1)      comment '在本行贴现标识'
    ,F130026    varchar(200)    comment '贴现客户名称'
    ,F130027    string          comment '贴现人账号'
    ,F130028    string          comment '贴现人开户行名称'
    ,F130029    decimal(16,2)   comment '贴现金额'
    ,F130030    varchar(10)     comment '贴现日期'
    ,F130031    int             comment '贴现计息天数'
    ,F130032    decimal(10,6)   comment '贴现利率'
    ,F130033    decimal(20,2)   comment '贴现利息'
    ,F130034    varchar(3)      comment '其他费用币种'
    ,F130035    decimal(20,2)   comment '其他费用金额'
    ,F130036    varchar(10)     comment '票据签发日期'
    ,F130037    varchar(10)     comment '票据到期日期'
    ,F130038    string          comment '对应的承兑业务协议号'
    ,F130039    varchar(1)      comment '代签承兑汇票标识'
    ,F130040    string          comment '代签承兑汇票的申请行的行名'
    ,F130041    string          comment '代签承兑汇票的开票行的行名'
    ,F130042    varchar(32)     comment '经办员工ID'
    ,F130043    varchar(32)     comment '审查员工ID'
    ,F130044    varchar(32)     comment '审批员工ID'
    ,F130045    varchar(1)      comment '或有负债标识'
    ,F130046    string          comment '贸易背景'
    ,F130047    varchar(2)      comment '票据状态'
    ,F130048    string          comment '备注'
    ,F130049    varchar(10)     comment '采集日期'
)
comment '6.13票据协议'

;

-- 6.14票据转贴现协议
drop table if exists T_6_14;
create table T_6_14(
     F140001    varchar(60)     comment '协议ID'
    ,F140002    varchar(24)     comment '机构ID'
    ,F140003    varchar(60)     comment '票据号码'
    ,F140004    varchar(2)      comment '票据类型'
    ,F140005    varchar(3)      comment '协议币种'
    ,F140006    decimal(16,2)   comment '票面金额'
    ,F140007    varchar(10)     comment '票据签发日期'
    ,F140008    varchar(10)     comment '票据到期日期'
    ,F140009    string          comment '出票人名称'
    ,F140010    string          comment '承兑人名称'
    ,F140011    varchar(200)    comment '贴现人名称'
    ,F140012    varchar(10)     comment '贴现日期'
    ,F140013    varchar(2)      comment '交易方向'
    ,F140014    varchar(2)      comment '转贴现类型'
    ,F140015    varchar(32)     comment '科目ID'
    ,F140016    string          comment '科目名称'
    ,F140017    varchar(10)     comment '转贴现日期'
    ,F140018    decimal(16,2)   comment '转贴现金额'
    ,F140019    int             comment '转贴现计息天数'
    ,F140020    decimal(10,6)   comment '转贴现利率'
    ,F140021    decimal(16,2)   comment '转贴现利息'
    ,F140022    varchar(10)     comment '回购日期'
    ,F140023    decimal(16,2)   comment '回购金额'
    ,F140024    decimal(10,6)   comment '回购利率'
    ,F140025    decimal(20,2)   comment '回购利息'
    ,F140026    string          comment '交易对手名称'
    ,F140027    string          comment '交易对手账号行号'
    ,F140028    varchar(3)      comment '重点产业标识'
    ,F140029    varchar(32)     comment '经办员工ID'
    ,F140030    varchar(32)     comment '审查员工ID'
    ,F140031    varchar(32)     comment '审批员工ID'
    ,F140032    varchar(1)      comment '或有负债标识'
    ,F140033    varchar(2)      comment '票据状态'
    ,F140034    string          comment '备注'
    ,F140035    varchar(10)     comment '采集日期'
)
comment '6.14票据转贴现协议'

;

-- 6.15房地产贷款协议
drop table if exists T_6_15;
create table T_6_15(
     F150001    varchar(60)     comment '协议ID'
    ,F150002    decimal(10,6)   comment '房地产开发贷款对应的项目资本金比例'
    ,F150003    decimal(16,2)   comment '房地产开发贷款对应的项目资本金金额'
    ,F150004    decimal(20,2)   comment '房地产开发贷款对应的项目投资额'
    ,F150005    varchar(2)      comment '商业用房购房贷款购买主体类型'
    ,F150006    int             comment '个人住房贷款对应的住房套数'
    ,F150007    decimal(16,2)   comment '贷款价值比'
    ,F150008    varchar(2)      comment '新建个人住房贷款标识'
    ,F150009    varchar(2)      comment '个人住房贷款利率分类标识'
    ,F150010    varchar(2)      comment '个人住房贷款基于贷款市场报价利率（LPR）标识'
    ,F150011    decimal(16,2)   comment '个人住房贷款对应房屋建筑面积'
    ,F150012    decimal(16,2)   comment '个人住房贷款偿债收入比'
    ,F150013    decimal(16,2)   comment '个人住房贷款首付金额'
    ,F150014    decimal(16,2)   comment '个人住房贷款对应房屋总价'
    ,F150015    decimal(16,2)   comment '个人住房贷款对应房地产押品市场价值'
    ,F150016    varchar(10)     comment '采集日期'
)
comment '6.15房地产贷款协议'

;

-- 6.16融资租赁协议
drop table if exists T_6_16;
create table T_6_16(
     F160001    varchar(60)     comment '协议ID'
    ,F160002    varchar(24)     comment '机构ID'
    ,F160003    varchar(2)      comment '融资租赁类型'
    ,F160004    varchar(2)      comment '融资租赁方式'
    ,F160005    varchar(32)     comment '租赁标的物'
    ,F160006    string          comment '承租人编号'
    ,F160007    varchar(200)    comment '承租人名称'
    ,F160008    string          comment '承租人账号'
    ,F160009    string          comment '承租人开户行名称'
    ,F160010    varchar(3)      comment '协议币种'
    ,F160011    decimal(20,2)   comment '合同金额'
    ,F160012    varchar(10)     comment '合同起始日期'
    ,F160013    varchar(10)     comment '合同到期日期'
    ,F160014    varchar(32)     comment '租赁公司名称'
    ,F160015    varchar(4)      comment '租赁公司证件类型'
    ,F160016    varchar(100)    comment '租赁公司证件号码'
    ,F160017    decimal(16,2)   comment '手续费金额'
    ,F160018    varchar(3)      comment '手续费币种'
    ,F160019    string          comment '保证金账号'
    ,F160020    varchar(3)      comment '保证金币种'
    ,F160021    decimal(16,2)   comment '保证金金额'
    ,F160022    decimal(4,2)    comment '保证金比例'
    ,F160023    varchar(3)      comment '重点产业标识'
    ,F160024    varchar(32)     comment '经办员工ID'
    ,F160025    varchar(32)     comment '审查员工ID'
    ,F160026    varchar(32)     comment '审批员工ID'
    ,F160027    string          comment '备注'
    ,F160028    varchar(10)     comment '采集日期'
)
comment '6.16融资租赁协议'

;

-- 6.17理财协议
drop table if exists T_6_17;
create table T_6_17(
     F170001    varchar(60)     comment '协议ID'
    ,F170002    varchar(24)     comment '机构ID'
    ,F170003    varchar(32)     comment '产品ID'
    ,F170004    varchar(60)     comment '客户ID'
    ,F170005    varchar(2)      comment '客户类型'
    ,F170006    varchar(2)      comment '客户风险偏好评估结果'
    ,F170007    varchar(10)     comment '生效日期'
    ,F170008    varchar(8)      comment '生效时间'
    ,F170009    varchar(10)     comment '到期日期'
    ,F170010    decimal(20,2)   comment '协议金额'
    ,F170011    varchar(3)      comment '协议币种'
    ,F170012    decimal(20,2)   comment '协议份额'
    ,F170013    varchar(2)      comment '销售渠道'
    ,F170014    varchar(2)      comment '业务类型'
    ,F170015    varchar(24)     comment '代销机构ID'
    ,F170016    string          comment '代销机构名称'
    ,F170017    varchar(2)      comment '代销机构所属监管机构'
    ,F170018    string          comment '关联存款账号'
    ,F170019    string          comment '关联存款账号开户行名称'
    ,F170020    varchar(2)      comment '关联存款账号开户所在地'
    ,F170021    varchar(2)      comment '协议状态'
    ,F170022    varchar(32)     comment '经办员工ID'
    ,F170023    string          comment '备注'
    ,F170024    decimal(20,2)   comment '手续费金额'
    ,F170025    varchar(10)     comment '采集日期'
)
comment '6.17理财协议'

;

-- 6.18委托贷款协议
drop table if exists T_6_18;
create table T_6_18(
     F180001    varchar(60)     comment '协议ID'
    ,F180002    varchar(24)     comment '机构ID'
    ,F180003    varchar(2)      comment '委托贷款类型'
    ,F180004    varchar(60)     comment '委托客户ID'
    ,F180005    string          comment '委托客户账号'
    ,F180006    string          comment '委托客户账号开户行名称'
    ,F180007    decimal(20,2)   comment '协议金额'
    ,F180008    varchar(3)      comment '协议币种'
    ,F180009    varchar(1)      comment '收息标识'
    ,F180010    varchar(10)     comment '生效日期'
    ,F180011    varchar(10)     comment '到期日期'
    ,F180012    varchar(60)     comment '借据ID'
    ,F180013    varchar(60)     comment '借款人ID'
    ,F180014    varchar(200)    comment '借款人名称'
    ,F180015    varchar(2)      comment '协议状态'
    ,F180016    varchar(32)     comment '科目ID'
    ,F180017    string          comment '科目名称'
    ,F180018    varchar(3)      comment '重点产业标识'
    ,F180019    varchar(32)     comment '经办员工ID'
    ,F180020    varchar(32)     comment '审查员工ID'
    ,F180021    varchar(32)     comment '审批员工ID'
    ,F180022    string          comment '备注'
    ,F180023    varchar(3)      comment '手续费币种'
    ,F180024    decimal(20,2)   comment '手续费金额'
    ,F180025    varchar(10)     comment '采集日期'
)
comment '6.18委托贷款协议'

;

-- 6.19代理协议
drop table if exists T_6_19;
create table T_6_19(
     F190001    varchar(60)     comment '协议ID'
    ,F190002    varchar(24)     comment '机构ID'
    ,F190003    varchar(60)     comment '委托人ID'
    ,F190004    varchar(200)    comment '委托人名称'
    ,F190005    varchar(2)      comment '委托人类型'
    ,F190006    varchar(2)      comment '代理产品类型'
    ,F190007    varchar(32)     comment '代理产品ID'
    ,F190008    string          comment '发行机构评级'
    ,F190009    string          comment '发行机构评级机构'
    ,F190010    varchar(200)    comment '融资人名称'
    ,F190011    varchar(5)      comment '融资人行业类型'
    ,F190012    varchar(10)     comment '签约日期'
    ,F190013    varchar(10)     comment '生效日期'
    ,F190014    varchar(10)     comment '到期日期'
    ,F190015    string          comment '本方清算账号'
    ,F190016    string          comment '对方清算账号'
    ,F190017    varchar(12)     comment '对方清算行号'
    ,F190018    varchar(32)     comment '经办员工ID'
    ,F190019    varchar(32)     comment '审查员工ID'
    ,F190020    varchar(32)     comment '审批员工ID'
    ,F190021    varchar(2)      comment '协议状态'
    ,F190022    string          comment '备注'
    ,F190023    varchar(10)     comment '采集日期'
)
comment '6.19代理协议'

;

-- 6.20代理销售协议
drop table if exists T_6_20;
create table T_6_20(
     F200001    varchar(60)     comment '协议ID'
    ,F200002    varchar(60)     comment '对应代理协议ID'
    ,F200003    varchar(24)     comment '机构ID'
    ,F200004    varchar(60)     comment '客户ID'
    ,F200005    varchar(2)      comment '客户类型'
    ,F200006    varchar(2)      comment '客户风险偏好评估结果'
    ,F200007    varchar(10)     comment '生效日期'
    ,F200008    varchar(8)      comment '生效时间'
    ,F200009    varchar(10)     comment '到期日期'
    ,F200010    decimal(20,2)   comment '协议金额'
    ,F200011    varchar(3)      comment '协议币种'
    ,F200012    varchar(32)     comment '经办员工ID'
    ,F200013    varchar(32)     comment '审查员工ID'
    ,F200014    varchar(32)     comment '审批员工ID'
    ,F200015    varchar(2)      comment '协议状态'
    ,F200016    string          comment '备注'
    ,F200017    varchar(10)     comment '采集日期'
)
comment '6.20代理销售协议'

;

-- 6.21投资协议
drop table if exists T_6_21;
create table T_6_21(
     F210001    varchar(60)     comment '协议ID'
    ,F210002    varchar(24)     comment '机构ID'
    ,F210003    varchar(60)     comment '交易对手ID'
    ,F210004    string          comment '交易对手名称'
    ,F210005    string          comment '交易对手账号'
    ,F210006    varchar(12)     comment '交易对手账号行号'
    ,F210007    varchar(10)     comment '签约日期'
    ,F210008    varchar(10)     comment '生效日期'
    ,F210009    varchar(2)      comment '收益类型'
    ,F210010    varchar(10)     comment '到期日期'
    ,F210011    varchar(3)      comment '协议币种'
    ,F210012    decimal(20,2)   comment '协议金额'
    ,F210013    string          comment '保证金账号'
    ,F210014    decimal(10,6)   comment '保证金比例'
    ,F210015    varchar(3)      comment '保证金币种'
    ,F210016    decimal(20,2)   comment '保证金金额'
    ,F210017    varchar(2)      comment '估值方法'
    ,F210018    varchar(2)      comment '资金来源'
    ,F210019    varchar(2)      comment '投资管理方式'
    ,F210020    varchar(60)     comment '投资标的ID'
    ,F210021    decimal(10,6)   comment '合同执行利率'
    ,F210022    varchar(1)      comment '含权标识'
    ,F210023    varchar(3)      comment '重点产业标识'
    ,F210024    varchar(32)     comment '经办员工ID'
    ,F210025    varchar(32)     comment '审查员工ID'
    ,F210026    varchar(32)     comment '审批员工ID'
    ,F210027    varchar(2)      comment '协议状态'
    ,F210028    varchar(1)      comment '或有负债标识'
    ,F210029    string          comment '备注'
    ,F210030    varchar(10)     comment '采集日期'
)
comment '6.21投资协议'

;

-- 6.22融资协议
drop table if exists T_6_22;
create table T_6_22(
     F220001    varchar(60)     comment '协议ID'
    ,F220002    varchar(24)     comment '机构ID'
    ,F220003    varchar(60)     comment '交易对手ID'
    ,F220004    string          comment '交易对手名称'
    ,F220005    string          comment '交易对手账号'
    ,F220006    varchar(12)     comment '交易对手账号行号'
    ,F220007    varchar(10)     comment '签约日期'
    ,F220008    varchar(10)     comment '生效日期'
    ,F220009    varchar(10)     comment '到期日期'
    ,F220010    varchar(3)      comment '协议币种'
    ,F220011    decimal(20,2)   comment '协议金额'
    ,F220012    varchar(2)      comment '成本类型'
    ,F220013    decimal(20,2)   comment '成本总额'
    ,F220014    varchar(32)     comment '对应融资产品ID'
    ,F220015    varchar(3)      comment '融资工具类型'
    ,F220016    varchar(2)      comment '融资工具子类型'
    ,F220017    varchar(2)      comment '押品类型'
    ,F220018    varchar(32)     comment '经办员工ID'
    ,F220019    varchar(32)     comment '审查员工ID'
    ,F220020    varchar(32)     comment '审批员工ID'
    ,F220021    varchar(2)      comment '协议状态'
    ,F220022    varchar(1)      comment '或有负债标识'
    ,F220023    string          comment '备注'
    ,F220024    varchar(10)     comment '采集日期'
)
comment '6.22融资协议'

;

-- 6.23信贷资产转让协议
drop table if exists T_6_23;
create table T_6_23(
     F230001    varchar(60)     comment '协议ID'
    ,F230002    varchar(24)     comment '机构ID'
    ,F230003    varchar(60)     comment '交易对手ID'
    ,F230004    string          comment '交易对手名称'
    ,F230005    string          comment '交易对手账号'
    ,F230006    varchar(12)     comment '交易对手账号行号'
    ,F230007    decimal(20,2)   comment '交易对手已支付金额'
    ,F230008    string          comment '转让价款入账账号'
    ,F230009    string          comment '转让价款入账账户名称'
    ,F230010    varchar(10)     comment '签约日期'
    ,F230011    varchar(10)     comment '生效日期'
    ,F230012    varchar(10)     comment '到期日期'
    ,F230013    varchar(3)      comment '协议币种'
    ,F230014    decimal(20,2)   comment '协议金额'
    ,F230015    varchar(2)      comment '交易资产类型'
    ,F230016    decimal(20,2)   comment '转让涉及业务本金总额'
    ,F230017    decimal(20,2)   comment '转让涉及业务利息总额'
    ,F230018    decimal(20,2)   comment '转让涉及业务笔数'
    ,F230019    decimal(20,2)   comment '保证金金额'
    ,F230020    varchar(3)      comment '保证金币种'
    ,F230021    decimal(10,6)   comment '保证金比例'
    ,F230022    varchar(2)      comment '转让交易平台'
    ,F230023    varchar(1)      comment '在银登中心登记标识'
    ,F230024    varchar(2)      comment '资产转让方向'
    ,F230025    varchar(2)      comment '资产转让方式'
    ,F230026    varchar(32)     comment '经办员工ID'
    ,F230027    varchar(32)     comment '审查员工ID'
    ,F230028    varchar(32)     comment '审批员工ID'
    ,F230029    varchar(2)      comment '协议状态'
    ,F230030    varchar(1)      comment '或有负债标识'
    ,F230031    string          comment '备注'
    ,F230032    varchar(10)     comment '采集日期'
)
comment '6.23信贷资产转让协议'

;

-- 6.24贷款承诺
drop table if exists T_6_24;
create table T_6_24(
     F240001    varchar(60)     comment '协议ID'
    ,F240002    varchar(100)    comment '业务号码'
    ,F240003    varchar(24)     comment '机构ID'
    ,F240004    varchar(60)     comment '客户ID'
    ,F240005    decimal(20,2)   comment '业务额度'
    ,F240006    varchar(3)      comment '币种'
    ,F240007    varchar(2)      comment '承诺类型'
    ,F240008    varchar(32)     comment '科目ID'
    ,F240009    string          comment '科目名称'
    ,F240010    decimal(20,2)   comment '未使用的额度'
    ,F240011    varchar(10)     comment '起始日期'
    ,F240012    varchar(10)     comment '到期日期'
    ,F240013    varchar(2)      comment '协议状态'
    ,F240014    varchar(3)      comment '重点产业标识'
    ,F240015    varchar(32)     comment '经办员工ID'
    ,F240016    varchar(32)     comment '审查员工ID'
    ,F240017    varchar(32)     comment '审批员工ID'
    ,F240018    varchar(10)     comment '采集日期'
)
comment '6.24贷款承诺'

;

-- 6.25互联网贷款合作协议
drop table if exists T_6_25;
create table T_6_25(
     F250001    varchar(24)     comment '机构ID'
    ,F250002    varchar(60)     comment '协议ID'
    ,F250003    string          comment '合作方名称'
    ,F250004    varchar(4)      comment '合作方证件类型'
    ,F250005    varchar(100)    comment '合作方证件号码'
    ,F250006    varchar(2)      comment '合作方类型'
    ,F250007    varchar(200)    comment '合作方式'
    ,F250008    varchar(2)      comment '提供增信的模式'
    ,F250009    varchar(6)      comment '合作方注册地行政区划'
    ,F250010    varchar(10)     comment '合作协议起始日期'
    ,F250011    varchar(10)     comment '合作协议到期日期'
    ,F250012    varchar(10)     comment '合作协议实际终止日期'
    ,F250013    varchar(1)      comment '限制标识'
    ,F250014    varchar(2)      comment '协议状态'
    ,F250015    string          comment '备注'
    ,F250016    varchar(10)     comment '采集日期'
)
comment '6.25互联网贷款合作协议'

;

-- 6.26其他协议
drop table if exists T_6_26;
create table T_6_26(
     F260001    varchar(60)     comment '协议ID'
    ,F260002    varchar(100)    comment '业务号码'
    ,F260003    varchar(24)     comment '机构ID'
    ,F260004    varchar(60)     comment '交易对手ID'
    ,F260005    varchar(32)     comment '产品ID'
    ,F260006    string          comment '交易对手名称'
    ,F260007    varchar(2)      comment '交易对手类型'
    ,F260008    string          comment '交易对手账号'
    ,F260009    varchar(12)     comment '交易对手账号行号'
    ,F260010    varchar(10)     comment '签约日期'
    ,F260011    varchar(10)     comment '生效日期'
    ,F260012    varchar(10)     comment '到期日期'
    ,F260013    varchar(128)    comment '其他协议币种'
    ,F260014    decimal(20,2)   comment '协议金额'
    ,F260015    varchar(2)      comment '协议义务'
    ,F260016    varchar(2)      comment '业务品种'
    ,F260017    string          comment '业务品种描述'
    ,F260018    varchar(1)      comment '信用风险仍在银行的销售与购买协议标识'
    ,F260019    varchar(1)      comment '其他担保类业务标识'
    ,F260020    varchar(3)      comment '重点产业标识'
    ,F260021    varchar(32)     comment '经办员工ID'
    ,F260022    varchar(32)     comment '审查员工ID'
    ,F260023    varchar(32)     comment '审批员工ID'
    ,F260024    varchar(2)      comment '协议状态'
    ,F260025    varchar(1)      comment '或有负债标识'
    ,F260026    string          comment '备注'
    ,F260027    varchar(10)     comment '采集日期'
)
comment ' 6.26其他协议'

;





-- 7.交易类数据    
-- 7.1客户转账交易
drop table if exists T_7_1;
create table T_7_1(
     G010001    varchar(100)    comment '交易ID'
    ,G010002    string          comment '分户账号'
    ,G010003    varchar(60)     comment '客户ID'
    ,G010004    varchar(24)     comment '交易机构ID'
    ,G010005    varchar(10)     comment '核心交易日期'
    ,G010006    varchar(8)      comment '核心交易时间'
    ,G010007    decimal(20,2)   comment '交易金额'
    ,G010008    decimal(20,2)   comment '账户余额'
    ,G010009    varchar(3)      comment '币种'
    ,G010010    varchar(2)      comment '转账交易类型'
    ,G010011    varchar(32)     comment '科目ID'
    ,G010012    string          comment '科目名称'
    ,G010013    varchar(2)      comment '现转标识'
    ,G010014    varchar(2)      comment '借贷标识'
    ,G010015    string          comment '对方账号'
    ,G010016    string          comment '对方户名'
    ,G010017    varchar(12)     comment '对方账号行号'
    ,G010018    string          comment '对方行名'
    ,G010019    string          comment '交易摘要'
    ,G010020    varchar(2)      comment '冲补抹标识'
    ,G010021    varchar(2)      comment '交易渠道'
    ,G010022    varchar(32)     comment '交易终端ID'
    ,G010023    varchar(15)     comment 'IP地址'
    ,G010024    varchar(12)     comment 'MAC地址'
    ,G010025    string          comment '外部账号（交易介质号）'
    ,G010026    varchar(200)    comment '代办人姓名'
    ,G010027    varchar(4)      comment '代办人证件类型'
    ,G010028    varchar(100)    comment '代办人证件号码'
    ,G010029    varchar(32)     comment '经办员工ID'
    ,G010030    varchar(32)     comment '授权员工ID'
    ,G010031    string          comment '客户备注'
    ,G010032    varchar(10)     comment '采集日期'
)
comment '7.1客户转账交易'

;

-- 7.2信贷交易
drop table if exists T_7_2;
create table T_7_2(
     G020001    varchar(100)    comment '交易ID'
    ,G020002    varchar(60)     comment '协议ID'
    ,G020003    string          comment '分户账号'
    ,G020004    varchar(60)     comment '客户ID'
    ,G020005    varchar(24)     comment '交易机构ID'
    ,G020006    varchar(60)     comment '借据ID'
    ,G020007    varchar(10)     comment '核心交易日期'
    ,G020008    varchar(8)      comment '核心交易时间'
    ,G020009    decimal(20,2)   comment '交易金额'
    ,G020010    decimal(20,2)   comment '账户余额'
    ,G020011    varchar(3)      comment '币种'
    ,G020012    varchar(2)      comment '信贷交易类型'
    ,G020013    varchar(32)     comment '科目ID'
    ,G020014    string          comment '科目名称'
    ,G020015    varchar(2)      comment '借贷标识'
    ,G020016    varchar(1)      comment '受托支付标识'
    ,G020017    string          comment '对方账号'
    ,G020018    string          comment '对方户名'
    ,G020019    varchar(12)     comment '对方账号行号'
    ,G020020    string          comment '对方行名'
    ,G020021    varchar(2)      comment '冲补抹标识'
    ,G020022    varchar(32)     comment '经办员工ID'
    ,G020023    varchar(32)     comment '授权员工ID'
    ,G020024    varchar(2)      comment '交易渠道'
    ,G020025    varchar(200)    comment '代办人姓名'
    ,G020026    varchar(4)      comment '代办人证件类型'
    ,G020027    varchar(100)    comment '代办人证件号码'
    ,G020028    varchar(2)      comment '现转标识'
    ,G020029    string          comment '摘要'
    ,G020030    varchar(10)     comment '采集日期'
)
comment '7.2信贷交易'

;

-- 7.3贸易融资交易
drop table if exists T_7_3;
create table T_7_3(
     G030001    varchar(100)    comment '交易ID'
    ,G030002    varchar(60)     comment '协议ID'
    ,G030003    string          comment '分户账号'
    ,G030004    varchar(60)     comment '客户ID'
    ,G030005    varchar(24)     comment '交易机构ID'
    ,G030006    varchar(10)     comment '核心交易日期'
    ,G030007    varchar(8)      comment '核心交易时间'
    ,G030008    varchar(2)      comment '交易类型'
    ,G030009    decimal(20,2)   comment '交易金额'
    ,G030010    varchar(3)      comment '币种'
    ,G030011    decimal(20,2)   comment '业务余额'
    ,G030012    string          comment '对方账号'
    ,G030013    string          comment '对方户名'
    ,G030014    varchar(12)     comment '对方账号行号'
    ,G030015    string          comment '对方行名'
    ,G030016    varchar(32)     comment '经办员工ID'
    ,G030017    varchar(32)     comment '授权员工ID'
    ,G030018    varchar(10)     comment '采集日期'
)
comment '7.3贸易融资交易'

;

-- 7.4信用卡交易
drop table if exists T_7_4;
create table T_7_4(
     G040001    varchar(100)    comment '交易ID'
    ,G040002    varchar(40)             comment '卡号'
    ,G040003    string          comment '分户账号'
    ,G040004    varchar(60)     comment '客户ID'
    ,G040005    varchar(24)     comment '机构ID'
    ,G040006    varchar(32)     comment '产品ID'
    ,G040007    varchar(10)     comment '核心交易日期'
    ,G040008    varchar(8)      comment '核心交易时间'
    ,G040009    varchar(2)      comment '交易类型'
    ,G040010    decimal(20,2)   comment '交易金额'
    ,G040011    decimal(20,2)   comment '账户余额'
    ,G040012    varchar(32)     comment '科目ID'
    ,G040013    string          comment '科目名称'
    ,G040014    decimal(20,2)   comment '手续费金额'
    ,G040015    varchar(3)      comment '币种'
    ,G040016    varchar(3)      comment '手续费币种'
    ,G040017    string          comment '对方账号'
    ,G040018    string          comment '对方户名'
    ,G040019    varchar(12)     comment '对方账号行号'
    ,G040020    string          comment '对方行名'
    ,G040021    varchar(2)      comment '借贷标识'
    ,G040022    varchar(100)    comment '商户编号'
    ,G040023    varchar(200)    comment '商户名称'
    ,G040024    varchar(2)      comment '线上线下交易标识'
    ,G040025    varchar(1)      comment '分期业务ID'
    ,G040026    varchar(15)     comment 'IP地址'
    ,G040027    varchar(12)     comment 'MAC地址'
    ,G040028    varchar(4)     comment '商户类别码'
    ,G040029    string          comment '商户类别码名称'
    ,G040030    varchar(2)      comment '交易渠道'
    ,G040031    string          comment '交易摘要'
    ,G040032    string          comment '客户备注'
    ,G040033    varchar(10)     comment '采集日期'
)
comment '7.4信用卡交易'

;

-- 7.5衍生品交易
drop table if exists T_7_5;
create table T_7_5(
     G050001    varchar(100)    comment '交易ID'
    ,G050002    varchar(24)     comment '交易机构ID'
    ,G050003    string          comment '交易机构名称'
    ,G050004    string          comment '交易账号'
    ,G050005    varchar(60)     comment '衍生品ID'
    ,G050006    varchar(2)      comment '交易类型'
    ,G050007    varchar(2)      comment '交易场所'
    ,G050008    varchar(10)     comment '交易日期'
    ,G050009    varchar(8)      comment '交易时间'
    ,G050010    varchar(32)     comment '科目ID'
    ,G050011    string          comment '科目名称'
    ,G050012    varchar(20)     comment '交割频率'
    ,G050013    decimal(20,2)   comment '标的数量'
    ,G050014    varchar(20)     comment '标的数量单位'
    ,G050015    decimal(20,2)   comment '成交价格'
    ,G050016    varchar(20)     comment '成交价格单位'
    ,G050017    varchar(2)      comment '交割方式'
    ,G050018    varchar(2)      comment '期权类型'
    ,G050019    decimal(20,2)   comment '行权价格'
    ,G050020    varchar(20)     comment '行权价格单位'
    ,G050021    varchar(1)      comment '保证金标识'
    ,G050022    varchar(60)     comment '主协议名称'
    ,G050023    string          comment '中央交易对手'
    ,G050024    varchar(2)      comment '交易状态'
    ,G050025    varchar(20)     comment '利率对'
    ,G050026    string          comment '交易对手名称'
    ,G050027    varchar(2)      comment '交易对手类别'
    ,G050028    varchar(20)     comment '交易对手评级'
    ,G050029    varchar(200)    comment '交易对手评级机构'
    ,G050030    varchar(12)     comment '交易对手账号行号'
    ,G050031    string          comment '交易对手账号'
    ,G050032    varchar(100)    comment '交易对手账号开户行名称'
    ,G050033    varchar(32)     comment '经办员工ID'
    ,G050034    varchar(32)     comment '审批员工ID'
    ,G050035    string          comment '备注'
    ,G050036    varchar(10)     comment '采集日期'
)
comment '7.5衍生品交易'

;

-- 7.6同业交易
drop table if exists T_7_6;
create table T_7_6(
     G060001    varchar(100)    comment '交易ID'
    ,G060002    varchar(60)     comment '同业业务ID'
    ,G060003    varchar(24)     comment '交易机构ID'
    ,G060004    string          comment '交易机构名称'
    ,G060005    string          comment '交易账号'
    ,G060006    decimal(20,2)   comment '交易金额'
    ,G060007    varchar(2)      comment '交易方向'
    ,G060008    varchar(3)      comment '币种'
    ,G060009    varchar(10)     comment '交易日期'
    ,G060010    varchar(8)      comment '交易时间'
    ,G060011    varchar(32)     comment '科目ID'
    ,G060012    string          comment '科目名称'
    ,G060013    string          comment '交易对手名称'
    ,G060014    varchar(2)      comment '交易对手类别'
    ,G060015    varchar(20)     comment '交易对手评级'
    ,G060016    varchar(200)    comment '交易对手评级机构'
    ,G060017    varchar(12)     comment '交易对手账号行号'
    ,G060018    string          comment '交易对手账号'
    ,G060019    varchar(100)    comment '交易对手账号开户行名称'
    ,G060020    varchar(2)      comment '是否为“调整后存贷比口径”的调整项'
    ,G060021    varchar(32)     comment '经办员工ID'
    ,G060022    varchar(32)     comment '审批员工ID'
    ,G060023    varchar(10)     comment '采集日期'
)
comment '7.6同业交易'

;

-- 7.7投资交易
drop table if exists T_7_7;
create table T_7_7(
     G070001    varchar(100)    comment '交易ID'
    ,G070002    varchar(24)     comment '交易机构ID'
    ,G070003    string          comment '交易机构名称'
    ,G070004    string          comment '交易账号'
    ,G070005    varchar(60)     comment '投资标的ID'
    ,G070006    decimal(20,2)   comment '交易金额'
    ,G070007    varchar(2)      comment '交易方向'
    ,G070008    varchar(3)      comment '币种'
    ,G070009    decimal(20,2)   comment '数量'
    ,G070010    decimal(20,2)   comment '单位成交净价'
    ,G070011    decimal(20,2)   comment '单位成交全价'
    ,G070012    varchar(2)      comment '资产计量方式'
    ,G070013    varchar(32)     comment '科目ID'
    ,G070014    string          comment '科目名称'
    ,G070015    varchar(10)     comment '交易日期'
    ,G070016    varchar(8)      comment '交易时间'
    ,G070017    string          comment '交易对手名称'
    ,G070018    varchar(2)      comment '交易对手类别'
    ,G070019    varchar(20)     comment '交易对手评级'
    ,G070020    varchar(200)    comment '交易对手评级机构'
    ,G070021    varchar(12)     comment '交易对手账号行号'
    ,G070022    string          comment '交易对手账号'
    ,G070023    varchar(100)    comment '交易对手账号开户行名称'
    ,G070024    varchar(32)     comment '经办员工ID'
    ,G070025    varchar(32)     comment '审批员工ID'
    ,G070026    string          comment '行内归属部门'
    ,G070027    varchar(32)     comment '产品ID'
    ,G070028    varchar(100)    comment '理财交易登记ID'
    ,G070029    varchar(100)    comment '行内理财交易ID'
    ,G070030    varchar(2)      comment '资金流动类型'
    ,G070032    string          comment '备注'
    ,G070031    varchar(10)     comment '采集日期'
    
)
comment '7.7投资交易'

;

-- 7.8不良资产处置
drop table if exists T_7_8;
create table T_7_8(
     G080001    varchar(100)    comment '交易ID'
    ,G080002    varchar(24)     comment '机构ID'
    ,G080003    varchar(60)     comment '借据ID'
    ,G080004    varchar(60)     comment '协议ID'
    ,G080005    varchar(60)     comment '客户ID'
    ,G080006    varchar(2)      comment '资产类型'
    ,G080007    varchar(2)      comment '处置类型'
    ,G080008    varchar(10)     comment '处置日期'
    ,G080009    decimal(20,2)   comment '处置时资产本金余额'
    ,G080010    decimal(20,2)   comment '处置时表内利息余额'
    ,G080011    decimal(20,2)   comment '处置时表外利息余额'
    ,G080012    decimal(20,2)   comment '处置后不良资产减少金额'
    ,G080013    decimal(20,2)   comment '处置收回资产金额'
    ,G080014    decimal(20,2)   comment '处置收回表内利息金额'
    ,G080015    decimal(20,2)   comment '处置收回表外利息金额'
    ,G080016    string          comment '转让资产名称'
    ,G080017    varchar(60)     comment '转让资产协议ID'
    ,G080018    varchar(2)      comment '收回标识'
    ,G080019    varchar(32)     comment '处置员工ID'
    ,G080020    varchar(10)     comment '处置收回日期'
    ,G080021    varchar(2)      comment '处置状态'
    ,G080022    varchar(3)      comment '币种'
    ,G080023    varchar(10)     comment '采集日期'
)
comment '7.8不良资产处置'

;

-- 7.9信贷资产转让
drop table if exists T_7_9;
create table T_7_9(
     G090001    varchar(60)     comment '协议ID'
    ,G090002    varchar(24)     comment '机构ID'
    ,G090003    varchar(60)     comment '借据ID'
    ,G090004    string          comment '转让价款入账账号'
    ,G090005    varchar(2)      comment '资产转让方向'
    ,G090006    varchar(2)      comment '资产转让方式'
    ,G090007    decimal(20,2)   comment '转让贷款本金总额'
    ,G090008    decimal(20,2)   comment '转让贷款利息总额'
    ,G090009    varchar(2)      comment '资产类型'
    ,G090010    varchar(10)     comment '核心交易日期'
    ,G090011    varchar(8)      comment '核心交易时间'
    ,G090012    string          comment '对方账号'
    ,G090013    string          comment '对方户名'
    ,G090014    varchar(12)     comment '对方账号行号'
    ,G090015    string          comment '对方行名'
    ,G090016    varchar(3)      comment '币种'
    ,G090017    decimal(20,2)   comment '交易对手已支付金额'
    ,G090018    varchar(10)     comment '采集日期'
)
comment '7.9信贷资产转让'

;

-- 7.10内部分户账交易
drop table if exists T_7_10;
create table T_7_10(
     G100001    varchar(100)    comment '交易ID'
    ,G100002    string          comment '分户账号'
    ,G100003    varchar(10)     comment '核心交易日期'
    ,G100004    varchar(8)      comment '核心交易时间'
    ,G100005    varchar(3)      comment '币种'
    ,G100006    varchar(2)      comment '交易类型'
    ,G100007    varchar(32)     comment '科目ID'
    ,G100008    string          comment '科目名称'
    ,G100009    varchar(2)      comment '借贷标识'
    ,G100010    decimal(20,2)   comment '交易金额'
    ,G100011    decimal(10,6)   comment '利率'
    ,G100012    decimal(20,2)   comment '借方余额'
    ,G100013    decimal(20,2)   comment '贷方余额'
    ,G100014    string          comment '对方账号'
    ,G100015    string          comment '对方户名'
    ,G100016    varchar(12)     comment '对方账号行号'
    ,G100017    string          comment '对方行名'
    ,G100018    string          comment '摘要'
    ,G100019    varchar(2)      comment '交易渠道'
    ,G100020    varchar(32)     comment '经办员工ID'
    ,G100021    varchar(32)     comment '授权员工ID'
    ,G100022    varchar(2)      comment '冲补抹标识'
    ,G100023    varchar(32)     comment '对方科目ID'
    ,G100024    string          comment '对方科目名称'
    ,G100025    varchar(2)      comment '现转标识'
    ,G100026    varchar(10)     comment '进账日期'
    ,G100027    varchar(10)     comment '销账日期'
    ,G100028    varchar(10)     comment '采集日期'
)
comment '7.10内部分户账交易'

;

-- 7.11理财及代销产品交易
drop table if exists T_7_11;
create table T_7_11(
     G110001    varchar(60)     comment '协议ID'
    ,G110002    varchar(60)     comment '客户ID'
    ,G110003    varchar(100)    comment '交易ID'
    ,G110004    varchar(2)      comment '销售渠道'
    ,G110005    varchar(10)     comment '销售日期'
    ,G110006    varchar(8)      comment '销售时间'
    ,G110007    string          comment '关联存款账号'
    ,G110008    string          comment '关联存款账号开户行名称'
    ,G110009    decimal(20,2)   comment '手续费金额'
    ,G110010    varchar(3)      comment '手续费币种'
    ,G110011    varchar(2)      comment '交易方向'
    ,G110012    varchar(2)      comment '现转标识'
    ,G110013    varchar(10)     comment '采集日期'
)
comment '7.11理财及代销产品交易'

;

-- 8.状态类数据    
-- 8.1贷款借据
drop table if exists T_8_1;
create table T_8_1(
     H010001    varchar(60)     comment '借据ID'
    ,H010002    varchar(60)     comment '客户ID'
    ,H010003    varchar(60)     comment '协议ID'
    ,H010004    varchar(24)     comment '机构ID'
    ,H010005    string          comment '分户账号'
    ,H010006    varchar(3)      comment '币种'
    ,H010007    varchar(32)     comment '科目ID'
    ,H010008    string          comment '科目名称'
    ,H010009    decimal(20,2)   comment '借款金额'
    ,H010010    decimal(20,2)   comment '借款余额'
    ,H010011    varchar(2)      comment '贷款发放类型'
    ,H010012    string          comment '贷款入账账号'
    ,H010013    string          comment '贷款入账户名'
    ,H010014    string          comment '入账账号所属行名称'
    ,H010015    string          comment '还款账号'
    ,H010016    string          comment '还款账号所属行名称'
    ,H010017    varchar(10)     comment '贷款实际发放日期'
    ,H010018    varchar(10)     comment '贷款实际到期日期'
    ,H010019    varchar(2)      comment '贷款状态'
    ,H010020    decimal(20,2)   comment '减值准备'
    ,H010021    decimal(10,6)   comment '贷款利率'
    ,H010022    string          comment '贷款用途'
    ,H010023    varchar(5)      comment '行业类型（按贷款投向划分）'
    ,H010024    varchar(3)      comment '重点产业标识'
    ,H010025    varchar(1)      comment '贷款逾期标识'
    ,H010026    varchar(2)      comment '是否为“调整后存贷比口径”的调整项'
    ,H010027    varchar(200)    comment '上笔信贷借据号'
    ,H010028    string          comment '备注'
    ,H010029    varchar(10)     comment '采集日期'
)
comment '8.1贷款借据'

;

-- 8.2信用证状态
drop table if exists T_8_2;
create table T_8_2(
     H020001    varchar(60)     comment '信用证ID'
    ,H020002    varchar(24)     comment '开票机构ID'
    ,H020003    varchar(32)     comment '科目ID'
    ,H020004    string          comment '科目名称'
    ,H020005    string          comment '议付交单机构'
    ,H020006    varchar(3)      comment '币种'
    ,H020007    decimal(20,2)   comment '已兑付金额'
    ,H020008    varchar(10)     comment '撤销日期'
    ,H020009    varchar(10)     comment '闭卷日期'
    ,H020010    decimal(20,2)   comment '押汇余额'
    ,H020011    decimal(20,2)   comment '垫款余额'
    ,H020012    varchar(2)      comment '合同状态'
    ,H020013    varchar(10)     comment '采集日期'
)
comment '8.2信用证状态'

;

-- 8.3垫款状态
drop table if exists T_8_3;
create table T_8_3(
     H030001    varchar(60)     comment '协议ID'
    ,H030002    varchar(60)     comment '客户ID'
    ,H030003    varchar(24)     comment '机构ID'
    ,H030004    varchar(60)     comment '借据ID'
    ,H030005    varchar(60)     comment '原协议ID'
    ,H030006    varchar(3)      comment '币种'
    ,H030007    varchar(2)      comment '垫款类型'
    ,H030008    decimal(20,2)   comment '垫款金额'
    ,H030009    decimal(20,2)   comment '垫款余额'
    ,H030010    varchar(10)     comment '垫款日期'
    ,H030011    varchar(2)      comment '垫款状态'
    ,H030012    string          comment '备注'
    ,H030013    varchar(10)     comment '采集日期'
)
comment '8.3垫款状态'

;

-- 8.4信用卡账户状态
drop table if exists T_8_4;
create table T_8_4(
     H040001    varchar(60)     comment '客户ID'
    ,H040002    varchar(40)     comment '卡号'
    ,H040003    string          comment '信用卡账号'
    ,H040004    varchar(24)     comment '开户机构ID'
    ,H040005    decimal(20,2)   comment '当前本币授信额度'
    ,H040006    decimal(20,2)   comment '当前外币授信额度'
    ,H040007    decimal(20,2)   comment '已使用本币授信额度'
    ,H040008    decimal(20,2)   comment '已使用外币授信额度'
    ,H040009    decimal(20,2)   comment '免息应收账款'
    ,H040010    decimal(20,2)   comment '应收息费'
    ,H040011    decimal(20,2)   comment '账户余额'
    ,H040012    decimal(20,2)   comment '逾期金额'
    ,H040013    varchar(3)      comment '币种'
    ,H040014    decimal(20,2)   comment '分期余额'
    ,H040015    varchar(6)      comment '五级分类'
    ,H040016    decimal(20,2)   comment '其中本币临时额度'
    ,H040017    decimal(20,2)   comment '其中外币临时额度'
    ,H040018    decimal(20,2)   comment '冻结金额'
    ,H040019    int             comment '当月累计交易笔数'
    ,H040020    decimal(20,2)   comment '当月累计透支金额'
    ,H040021    decimal(20,2)   comment '本月累计消费金额'
    ,H040022    decimal(20,2)   comment '本月累计取现转账金额'
    ,H040023    decimal(20,2)   comment '本月累计分期交易金额'
    ,H040024    decimal(20,2)   comment '本月累计收入'
    ,H040025    decimal(20,2)   comment '当年累计信用卡收入金额'
    ,H040026    int             comment '已有信用卡发卡银行数'
    ,H040027    decimal(20,2)   comment '已有他行授信金额'
    ,H040028    varchar(1)      comment '催收标识'
    ,H040029    string          comment '催收方式'
    ,H040030    varchar(2)      comment '新增授信类型'
    ,H040031    varchar(10)     comment '逾期起始日期'
    ,H040032    varchar(10)     comment '最近授信评估日期'
    ,H040033    varchar(10)     comment '最近征信查询日期'
    ,H040034    varchar(10)     comment '最近新增授信日期'
    ,H040035    varchar(10)     comment '最后交易日期'
    ,H040036    varchar(10)     comment '采集日期'
)
comment '8.4信用卡账户状态'

;

-- 8.5信用卡分期状态
drop table if exists T_8_5;
create table T_8_5(
     H050001    varchar(60)     comment '分期业务ID'
    ,H050002    varchar(40)     comment '卡号'
    ,H050003    varchar(100)    comment '交易ID'
    ,H050004    varchar(60)     comment '客户ID'
    ,H050005    varchar(2)      comment '分期交易类型'
    ,H050006    varchar(2)      comment '分期业务类型'
    ,H050007    varchar(3)      comment '币种'
    ,H050008    decimal(20,2)   comment '分期总额度'
    ,H050009    decimal(20,2)   comment '可用分期额度'
    ,H050010    decimal(20,2)   comment '分期金额'
    ,H050011    int             comment '分期期数'
    ,H050012    decimal(10,6)   comment '分期利率'
    ,H050013    varchar(10)     comment '办理分期日期'
    ,H050014    varchar(6)      comment '办理分期时间'
    ,H050015    varchar(40)     comment '分期转入卡号'
    ,H050016    varchar(1)      comment '个性化分期标识'
    ,H050017    varchar(1)      comment '提前结清标识'
    ,H050018    varchar(10)     comment '采集日期'
)
comment '8.5信用卡分期状态'

;

-- 8.6衍生品存量情况
drop table if exists T_8_6;
create table T_8_6(
     H060001    varchar(60)     comment '衍生品ID'
    ,H060002    varchar(24)     comment '交易机构ID'
    ,H060003    varchar(60)     comment '协议ID'
    ,H060004    string          comment '衍生品名称'
    ,H060005    varchar(2)      comment '衍生品类型'
    ,H060006    varchar(200)    comment '基础资产名称'
    ,H060007    varchar(2)      comment '基础资产类型'
    ,H060008    varchar(2)      comment '账户类型'
    ,H060009    varchar(3)      comment '币种'
    ,H060010    decimal(20,2)   comment '正总市场价值'
    ,H060011    decimal(20,2)   comment '负总市场价值'
    ,H060012    varchar(10)     comment '估值日期'
    ,H060013    decimal(20,2)   comment '多头头寸'
    ,H060014    decimal(20,2)   comment '空头头寸'
    ,H060015    varchar(10)     comment '合同起始日期'
    ,H060016    varchar(10)     comment '合同终止日期'
    ,H060017    varchar(10)     comment '衍生品发行日期'
    ,H060018    varchar(10)     comment '衍生品到期日期'
    ,H060019    varchar(32)     comment '科目ID'
    ,H060020    string          comment '科目名称'
    ,H060021    varchar(3)      comment '国家地区'
    ,H060022    varchar(2)      comment '行权方式'
    ,H060023    varchar(3)      comment '本方初始币种'
    ,H060024    varchar(3)      comment '对方初始币种'
    ,H060025    varchar(2)      comment '本方利率类型'
    ,H060026    varchar(2)      comment '对方利率类型'
    ,H060027    varchar(32)     comment '本方利率基准'
    ,H060028    decimal(10,6)   comment '本方利率浮动点'
    ,H060029    varchar(32)     comment '对方利率基准'
    ,H060030    decimal(10,6)   comment '对方利率浮动点'
    ,H060031    varchar(60)     comment '担保协议ID'
    ,H060032    varchar(10)     comment '采集日期'
)
comment '8.6衍生品存量情况'

;

-- 8.7同业存量情况
drop table if exists T_8_7;
create table T_8_7(
     H070001    varchar(60)     comment '同业业务ID'
    ,H070002    varchar(24)     comment '交易机构ID'
    ,H070003    varchar(60)     comment '协议ID'
    ,H070004    varchar(2)      comment '同业业务种类'
    ,H070005    varchar(32)     comment '科目ID'
    ,H070006    string          comment '科目名称'
    ,H070007    varchar(2)      comment '账户类型'
    ,H070008    decimal(20,2)   comment '合同金额'
    ,H070009    decimal(20,2)   comment '合同余额'
    ,H070010    varchar(3)      comment '币种'
    ,H070011    varchar(10)     comment '合同起始日期'
    ,H070012    varchar(10)     comment '合同终止日期'
    ,H070013    decimal(10,6)   comment '合同执行利率'
    ,H070014    varchar(2)      comment '业务目的'
    ,H070015    varchar(60)     comment '担保协议ID'
    ,H070016    varchar(60)     comment '投资标的ID'
    ,H070017    varchar(10)     comment '采集日期'
)
comment '8.7同业存量情况'

;

-- 8.8投资情况
drop table if exists T_8_8;
create table T_8_8(
     H080001    varchar(60)     comment '投资标的ID'
    ,H080002    string          comment '投资产品名称'
    ,H080003    varchar(24)     comment '交易机构ID'
    ,H080004    varchar(60)     comment '协议ID'
    ,H080005    string          comment '交易账号'
    ,H080006    varchar(2)      comment '账户类型'
    ,H080007    varchar(32)     comment '产品ID'
    ,H080008    varchar(32)     comment '科目ID'
    ,H080009    string          comment '科目名称'
    ,H080010    varchar(2)      comment '管理方式'
    ,H080011    decimal(20,2)   comment '投资余额'
    ,H080012    varchar(3)      comment '投资标的币种'
    ,H080013    decimal(20,2)   comment '本期投资收益'
    ,H080014    decimal(20,2)   comment '累计投资收益'
    ,H080015    decimal(20,2)   comment '持有成本'
    ,H080016    varchar(60)     comment '担保协议ID'
    ,H080017    varchar(10)     comment '采集日期'
)
comment '8.8投资情况'

;

-- 8.9融资情况
drop table if exists T_8_9;
create table T_8_9(
     H090001    varchar(60)     comment '协议ID'
    ,H090002    varchar(32)     comment '产品ID'
    ,H090003    varchar(2)      comment '融资工具类型'
    ,H090004    varchar(3)      comment '融资工具子类型'
    ,H090005    decimal(20,2)   comment '合同金额'
    ,H090006    varchar(3)      comment '币种'
    ,H090007    decimal(20,2)   comment '融资余额'
    ,H090008    decimal(10,6)   comment '合同执行利率'
    ,H090009    varchar(60)     comment '担保协议ID'
    ,H090010    varchar(10)     comment '采集日期'
)
comment '8.9融资情况'

;

-- 8.10理财产品状态
drop table if exists T_8_10;
create table T_8_10(
     H100001    varchar(32)     comment '产品ID'
    ,H100002    decimal(20,2)   comment '累计申购金额'
    ,H100003    decimal(20,2)   comment '累计申购份额'
    ,H100004    decimal(20,2)   comment '累计兑付金额'
    ,H100005    decimal(20,2)   comment '累计兑付收益金额'
    ,H100006    decimal(20,2)   comment '累计赎回份额'
    ,H100007    decimal(20,2)   comment '产品存续余额'
    ,H100008    decimal(20,2)   comment '初始净值'
    ,H100009    decimal(20,2)   comment '产品净值'
    ,H100010    decimal(20,2)   comment '累计净值'
    ,H100011    varchar(3)      comment '净值币种'
    ,H100012    decimal(20,2)   comment '折算人民币初始净值'
    ,H100013    decimal(20,2)   comment '折算人民币净值'
    ,H100014    decimal(20,2)   comment '折算人民币累计净值'
    ,H100015    decimal(10,6)   comment '实现收益率'
    ,H100016    decimal(20,2)   comment '银行实现收益'
    ,H100017    decimal(10,6)   comment '理财产品杠杆率'
    ,H100018    decimal(20,2)   comment '理财产品总资产金额'
    ,H100019    decimal(20,2)   comment '穿透后资产余额'
    ,H100020    decimal(20,2)   comment '穿透后负债余额'
    ,H100021    decimal(20,2)   comment '自然人持有余额'
    ,H100022    decimal(20,2)   comment '非金融机构持有余额'
    ,H100023    decimal(20,2)   comment '银行类金融机构持有余额'
    ,H100024    decimal(20,2)   comment '其他金融机构持有余额'
    ,H100025    varchar(10)     comment '采集日期'
)
comment '8.10理财产品状态'

;

-- 8.11表外业务手续费及收益
drop table if exists T_8_11;
create table T_8_11(
     H110001    varchar(24)     comment '机构ID'
    ,H110002    varchar(60)     comment '协议ID'
    ,H110003    varchar(2)      comment '业务类型'
    ,H110004    decimal(20,2)   comment '业务余额'
    ,H110005    decimal(20,2)   comment '本年累计发生额'
    ,H110006    varchar(32)     comment '产品ID'
    ,H110007    decimal(20,2)   comment '累计实现产品收益'
    ,H110008    decimal(20,2)   comment '累计实现银行端收益'
    ,H110009    decimal(20,2)   comment '累计实现客户端收益'
    ,H110010    varchar(2)      comment '手续费计算方式'
    ,H110011    decimal(20,2)   comment '手续费总额'
    ,H110012    varchar(2)      comment '手续费收取方式'
    ,H110013    varchar(10)     comment '采集日期'
)
comment '8.11表外业务手续费及收益'

;

-- 8.12五级分类状态
drop table if exists T_8_12;
create table T_8_12(
     H120001    varchar(60)     comment '协议ID'
    ,H120002    varchar(60)     comment '借据ID'
    ,H120003    varchar(24)     comment '机构ID'
    ,H120004    varchar(10)     comment '调整日期'
    ,H120005    varchar(6)      comment '当前五级分类'
    ,H120006    varchar(6)      comment '原五级分类'
    ,H120007    varchar(2)      comment '变动方式'
    ,H120008    string          comment '变动原因'
    ,H120009    varchar(32)     comment '经办员工ID'
    ,H120010    varchar(32)     comment '审查员工ID'
    ,H120011    varchar(32)     comment '审批员工ID'
    ,H120012    varchar(3)      comment '币种'
    ,H120014    decimal(20,2)   comment '减值准备'
    ,H120013    varchar(10)     comment '采集日期'
)
comment '8.12五级分类状态'

;

-- 8.13授信情况
drop table if exists T_8_13;
create table T_8_13(
     H130001    varchar(64)     comment '授信ID'
    ,H130002    varchar(60)     comment '客户ID'
    ,H130003    varchar(24)     comment '机构ID'
    ,H130004    varchar(60)     comment '协议ID'
    ,H130005    varchar(2)      comment '客户类别'
    ,H130006    varchar(2)      comment '授信种类'
    ,H130007    varchar(3)      comment '授信币种'
    ,H130008    decimal(20,2)   comment '授信额度'
    ,H130009    decimal(20,2)   comment '非保本理财产品授信额度'
    ,H130010    varchar(10)     comment '额度申请日期'
    ,H130011    varchar(10)     comment '授信起始日期'
    ,H130012    varchar(10)     comment '授信到期日期'
    ,H130013    decimal(20,2)   comment '持有债券余额'
    ,H130014    decimal(20,2)   comment '持有股权余额'
    ,H130015    decimal(20,2)   comment '表内用信余额'
    ,H130016    decimal(20,2)   comment '表外用信余额'
    ,H130017    decimal(20,2)   comment '不考虑风险缓释季末风险暴露金额'
    ,H130018    decimal(20,2)   comment '考虑风险缓释季末风险暴露金额'
    ,H130019    string          comment '授信审批意见'
    ,H130020    varchar(32)     comment '经办员工ID'
    ,H130021    varchar(32)     comment '审批员工ID'
    ,H130022    varchar(1)      comment '授信状态'
    ,H130023    varchar(10)     comment '采集日期'
)
comment '8.13授信情况'

;

-- 8.14存款状态
drop table if exists T_8_14;
create table T_8_14(
     H140001    string          comment '存款账号'
    ,H140002    varchar(60)     comment '客户ID'
    ,H140003    varchar(24)     comment '机构ID'
    ,H140004    varchar(3)      comment '币种'
    ,H140005    varchar(32)     comment '科目ID'
    ,H140006    string          comment '科目名称'
    ,H140007    varchar(2)      comment '交易介质'
    ,H140008    varchar(40)     comment '交易介质号'
    ,H140009    varchar(2)      comment '存款期限'
    ,H140010    decimal(10,6)   comment '利率'
    ,H140011    varchar(10)     comment '开户日期'
    ,H140012    varchar(10)     comment '销户日期'
    ,H140013    decimal(20,2)   comment '存款余额'
    ,H140014    varchar(2)      comment '账户状态'
    ,H140016    varchar(10)     comment '上次动户日期'    
    ,H140015    varchar(10)     comment '采集日期'
)
comment '8.14存款状态'

;

-- 8.15还款状态
drop table if exists T_8_15;
create table T_8_15(
     H150001    varchar(60)     comment '客户ID'
    ,H150002    varchar(60)     comment '协议ID'
    ,H150003    varchar(60)     comment '借据ID'
    ,H150004    varchar(2)      comment '还本方式'
    ,H150005    varchar(2)      comment '还息方式'
    ,H150006    int             comment '本期还款期数'
    ,H150007    int             comment '计划还款期数'
    ,H150008    varchar(10)     comment '本期计划还款日期'
    ,H150009    decimal(20,2)   comment '本期计划归还本金金额'
    ,H150010    decimal(20,2)   comment '本期计划归还利息金额'
    ,H150011    decimal(20,2)   comment '本期已归还本金'
    ,H150012    decimal(20,2)   comment '本期已归还利息'
    ,H150013    int             comment '累计展期次数'
    ,H150014    int             comment '连续欠本天数'
    ,H150015    int             comment '连续欠息天数'
    ,H150016    int             comment '累积欠本天数'
    ,H150017    int             comment '累积欠息天数'
    ,H150018    int             comment '连续欠款期数'
    ,H150019    int             comment '累计欠款期数'
    ,H150020    decimal(20,2)   comment '欠本金额'
    ,H150021    decimal(20,2)   comment '表内欠款利息'
    ,H150022    decimal(20,2)   comment '表外欠款利息'
    ,H150023    varchar(10)     comment '欠本日期'
    ,H150024    varchar(10)     comment '欠息日期'
    ,H150025    varchar(10)     comment '终结日期'
    ,H150026    varchar(10)     comment '采集日期'
)
comment '8.15还款状态'

;

-- 8.16客户理财产品持有状态
drop table if exists T_8_16;
create table T_8_16(
     H160001   varchar(60)     comment '客户ID'
    ,H160002   varchar(32) 	   comment '产品ID'
    ,H160003   varchar(3)      comment '币种'
    ,H160004   decimal(20,2)   comment '客户持有理财余额'
    ,H160005   decimal(20,2)   comment '客户持有理财折算人民币余额'
    ,H160006   decimal(16,2)   comment '客户持有理财份额'
    ,H160008   varchar(10)     comment '客户持有日期'
    ,H160007   varchar(10)     comment '采集日期'

)
comment '8.16客户理财产品持有状态'

;

-- 9.资源类数据    
-- 9.1投资标的关系
drop table if exists T_9_1;
create table T_9_1(
     J010001    varchar(60)     comment '投资标的ID'
    ,J010002    varchar(24)     comment '机构ID'
    ,J010003    varchar(32)     comment '产品ID'
    ,J010004    varchar(60)     comment '上一层投资标的ID'
    ,J010005    decimal(10,6)   comment '占上一层投资标的比例'
    ,J010006    decimal(20,2)   comment '产品持有底层资产折算人民币金额'
    ,J010007    decimal(20,2)   comment '理财产品持有底层资产折算人民币金额（理财中心）'
    ,J010008    decimal(16,2)   comment '产品持有底层资产份额'
    ,J010009    decimal(16,2)   comment '理财产品持有底层资产份额（理财中心）'
    ,J010010    varchar(2)      comment '直接或间接投资标识'
    ,J010011    int             comment '投资标的层级'
    ,J010012    int             comment '产品总层级'
    ,J010013    string          comment '备注'
    ,J010014    varchar(10)     comment '采集日期'
)
comment '9.1投资标的关系'

;

-- 9.2投资标的
drop table if exists T_9_2;
create table T_9_2(
     J020001    varchar(60)     comment '投资标的ID'
    ,J020002    string          comment '投资标的名称'
    ,J020003    varchar(24)     comment '机构ID'
    ,J020004    decimal(20,2)   comment '发行价格'
    ,J020005    decimal(20,2)   comment '发行规模'
    ,J020006    string          comment '发行机构名称'
    ,J020007    varchar(18)      comment '发行机构代码'
    ,J020008    varchar(2)      comment '发行机构类型'
    ,J020009    varchar(3)      comment '发行国家地区'
    ,J020010    varchar(128)    comment '投资标的币种'
    ,J020011    string          comment '投资标的代码'
    ,J020012    string          comment '行内资产/负债编码'
    ,J020013    string          comment '资产负债登记编码'
    ,J020014    varchar(10)     comment '起息日期'
    ,J020015    varchar(10)     comment '发行日期'
    ,J020016    varchar(10)     comment '到期日期'
    ,J020017    varchar(2)      comment '投资标的利率类型'
    ,J020018    decimal(10,6)   comment '利率/收益率'
    ,J020019    decimal(20,2)   comment '最近评估价格'
    ,J020020    varchar(10)     comment '评估价格日期'
    ,J020021    varchar(2)      comment '投资标的类别'
    ,J020022    decimal(10,6)   comment '资产风险权重'
    ,J020023    varchar(2)      comment '自营业务大类'
    ,J020024    varchar(2)      comment '自营业务中类'
    ,J020025    varchar(4)      comment '自营业务小类'
    ,J020026    string          comment '基础资产客户名称'
    ,J020027    varchar(3)      comment '基础资产客户国家'
    ,J020028    string          comment '基础资产客户评级'
    ,J020029    string          comment '基础资产客户评级机构'
    ,J020030    varchar(5)      comment '基础资产客户行业类型'
    ,J020031    string          comment '基础资产评级'
    ,J020032    string          comment '基础资产评级机构'
    ,J020033    varchar(2)      comment '基础资产最终投向类型'
    ,J020034    varchar(5)      comment '基础资产最终投向行业类型'
    ,J020035    varchar(2)      comment '一级资产类别'
    ,J020036    varchar(4)      comment '二级资产类别'
    ,J020037    varchar(3)      comment '理财标的投向'
    ,J020038    varchar(2)      comment '三级资产类别'
    ,J020039    varchar(3)      comment '估值币种'
    ,J020040    decimal(20,2)   comment '单位资产估值（净价）'
    ,J020041    decimal(20,2)   comment '单位资产估值（全价）'
    ,J020042    varchar(2)      comment '交易流通场所'
    ,J020043    int             comment '剩余期限'
    ,J020044    varchar(100)    comment '资产ID'
    ,J020045    string          comment '资产评级'
    ,J020046    varchar(200)    comment '融资人名称'
    ,J020047    varchar(128)    comment '融资人统一社会信用代码'
    ,J020048    string          comment '融资人内部评级'
    ,J020049    varchar(2)      comment '融资人类型（按规模划分）'
    ,J020050    varchar(2)      comment '融资人类型（按技术领域划分）'
    ,J020051    varchar(2)      comment '融资人类型（按经济类型划分）'
    ,J020052    string          comment '融资项目名称'
    ,J020053    varchar(5)      comment '融资人行业类型'
    ,J020054    varchar(3)      comment '融资项目所属国家地区'
    ,J020055    varchar(5)      comment '融资项目行业类型'
    ,J020056    varchar(1)      comment '融资项目属于重点监控行业和领域标识'
    ,J020057    varchar(2)      comment '重点监控行业和领域类别'
    ,J020058    varchar(2)      comment '主要担保方式'
    ,J020059    varchar(200)    comment '担保说明'
    ,J020060    varchar(3)      comment '抵质押物类型'
    ,J020061    decimal(20,2)   comment '抵质押物价值'
    ,J020062    varchar(2)      comment '担保性质'
    ,J020063    varchar(2)      comment '担保人与融资人关系'
    ,J020064    varchar(60)     comment '押品ID'
    ,J020065    varchar(60)     comment '担保协议ID'
    ,J020066    decimal(16,2)  comment '付息频率'
    ,J020067    string          comment '资产外部评级'
    ,J020068    varchar(2)      comment '收/受益权类型'
    ,J020069    varchar(1)      comment '买入返售标识'
    ,J020070    decimal(20,2)   comment '份额面值'
    ,J020071    varchar(2)      comment '计息类型'
    ,J020072    int             comment '计息基础'
    ,J020073    varchar(1)      comment '规则付息标识'
    ,J020074    varchar(2)      comment '利息分布方式'
    ,J020075    varchar(2)      comment '基准利率种类'
    ,J020076    varchar(1)      comment '浮动因子标识'
    ,J020077    decimal(10,6)   comment '浮动因子'
    ,J020078    varchar(2)      comment '结构档次'
    ,J020079    varchar(2)      comment '还本方式'
    ,J020080    varchar(2)      comment '分期还本条款标识'
    ,J020081    decimal(10,6)   comment '超额收益分配比例'
    ,J020082    decimal(10,6)   comment '利差'
    ,J020083    string          comment '增信机构代码'
    ,J020084    string          comment '增信机构名称'
    ,J020085    string          comment '融资人外部评级'
    ,J020086    string          comment '资产内部评级'
    ,J020087    varchar(2)      comment '含权类型'
    ,J020088    string          comment '选择权'
    ,J020089    varchar(2)      comment '行权方式'
    ,J020090    string          comment '行权条件说明'
    ,J020091    varchar(10)     comment '固定行权日期'
    ,J020092    varchar(10)     comment '首次行权日期'
    ,J020093    int             comment '行权周期'
    ,J020094    decimal(20,2)   comment '行权价格'
    ,J020095    varchar(2)      comment '永续条款类型'
    ,J020096    varchar(2)      comment '利息递延条款类型'
    ,J020097    varchar(1)      comment '递延利息计息标识'
    ,J020098    varchar(10)     comment '首次重定价日期'
    ,J020099    int             comment '重定价周期'
    ,J020100    varchar(1)      comment '部分赎回标识'
    ,J020101    decimal(10,6)   comment '部分赎回比例'
    ,J020102    string          comment '费用情况说明'
    ,J020103    varchar(1)      comment '存在变现障碍标识'
    ,J020104    string          comment '备注'
    ,J020106    string          comment '还本付息情况说明'
    ,J020105    varchar(10)     comment '采集日期'

)
comment '9.2投资标的'

;

-- 9.3抵质押品
drop table if exists T_9_3;
create table T_9_3(
     J030001    varchar(60)     comment '押品ID'
    ,J030002    varchar(60)     comment '担保协议ID'
    ,J030003    varchar(24)     comment '机构ID'
    ,J030004    varchar(32)     comment '产品ID'
    ,J030005    varchar(3)      comment '抵质押物类型'
    ,J030006    string          comment '抵质押物名称'
    ,J030007    varchar(2)      comment '抵质押物状态'
    ,J030008    decimal(20,2)   comment '起始估值'
    ,J030009    varchar(3)      comment '币种'
    ,J030010    decimal(20,2)   comment '最新估值'
    ,J030011    varchar(10)     comment '首次估值日期'
    ,J030012    varchar(10)     comment '最新估值日期'
    ,J030013    varchar(10)     comment '估值到期日期'
    ,J030014    varchar(1)      comment '对应唯一担保协议标识'
    ,J030015    varchar(1)      comment '抵押顺位'
    ,J030016    varchar(200)    comment '抵质押物所有权人名称'
    ,J030017    varchar(4)      comment '抵质押物所有权人证件类型'
    ,J030018    varchar(100)    comment '抵质押物所有权人证件号码'
    ,J030019    decimal(20,2)   comment '已抵押价值'
    ,J030020    decimal(10,6)   comment '审批抵质押率'
    ,J030021    decimal(10,6)   comment '抵质押率'
    ,J030022    varchar(10)     comment '登记日期'
    ,J030023    string          comment '登记机构'
    ,J030024    varchar(2)      comment '质押票证类型'
    ,J030025    string          comment '质押票证号码'
    ,J030026    string          comment '质押票证签发机构'
    ,J030027    varchar(2)      comment '权证种类'
    ,J030028    string          comment '权证登记号码'
    ,J030029    string          comment '权证登记面积'
    ,J030030    varchar(1)      comment '纳入合格优质流动性资产储备标识'
    ,J030031    decimal(10,6)   comment '可被无条件替换的比例'
    ,J030032    varchar(1)      comment '触及预警线标识'
    ,J030033    varchar(1)      comment '触及平仓线标识'
    ,J030034    varchar(2)      comment '交易场所'
    ,J030035    string          comment '股票股数'
    ,J030036    string          comment '备注'
    ,J030037    varchar(10)     comment '采集日期'
)
comment '9.3抵质押品'

;

-- 9.4商业单据
drop table if exists T_9_4;
create table T_9_4(
     J040001    varchar(60)     comment '单据ID'
    ,J040002    varchar(24)     comment '机构ID'
    ,J040003    varchar(60)     comment '开票人客户ID'
    ,J040004    varchar(3)      comment '商业单据币种'
    ,J040005    decimal(20,2)   comment '商业单据金额'
    ,J040006    varchar(2)      comment '商业单据种类'
    ,J040007    string          comment '备注'
    ,J040008    varchar(10)     comment '采集日期'
)
comment '9.4商业单据'

;

-- 10.参数类数据    
-- 10.1公共代码
drop table if exists T_10_1;
create table T_10_1(
     K010001   varchar(8)   comment '参数ID'
    ,K010002   string       comment '表名'
    ,K010003   string       comment '字段名'
    ,K010004   string       comment '代码'
    ,K010005   string       comment '中文含义'
)
comment '10.1公共代码'

;

-- 10.2汇率利率
drop table if exists T_10_2;
create table T_10_2(
     K020001   varchar(24)     comment '机构ID'
    ,K020002   varchar(14)     comment '汇率ID'
    ,K020003   varchar(3)      comment '外币币种'
    ,K020004   varchar(3)      comment '本币币种'
    ,K020005   decimal(20,2)   comment '中间价'
    ,K020006   decimal(20,2)   comment '基准价'
    ,K020007   decimal(10,6)   comment '基准（LPR）利率（一年期）'
    ,K020008   decimal(10,6)   comment '基准（LPR）利率（五年期）'
    ,K020009   varchar(10)     comment '采集日期'
)
comment '10.2汇率利率'

;


