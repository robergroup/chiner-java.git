SELECT
    t.table_name AS tbl_name,
    c.comments AS tbl_comment,
    t.OWNER AS db_name
FROM all_tables t left join all_tab_comments c on t.OWNER=c.OWNER and t.TABLE_NAME=c.TABLE_NAME
where t.OWNER='SYSDBA' AND t.table_name not like '##%';

SELECT
    col.table_name AS tbl_name,
    '' AS tbl_comment,
    col.column_name AS col_name,
    clc.comments AS col_comment,
    col.data_type AS data_type,
    col.data_length as data_length,
    col.data_precision AS data_precision,
    col.data_scale AS data_scale,
    col.nullable AS is_nullable,
    cpk.constraint_type AS is_primary_key,
    col.data_default AS default_value
FROM
    all_tab_columns col
        LEFT JOIN all_col_comments clc ON col.owner = clc.owner AND col.table_name = clc.table_name  AND col.column_name = clc.column_name
        LEFT JOIN (select cst.owner,cst.constraint_name,csc.table_name,csc.column_name,cst.constraint_type
                   from all_constraints cst,all_cons_columns csc
                   where 1=1
                     and cst.owner = csc.owner
                     and cst.constraint_name=csc.constraint_name
                     and   cst.constraint_type ='P'
                     and csc.table_name not like '%$%'
    ) cpk on col.owner = cpk.owner and col.table_name = cpk.table_name  AND col.column_name = cpk.column_name
WHERE
        col.OWNER = 'SYSDBA'
  AND UPPER(col.table_name) = 'SIMS_STUDENT'
ORDER BY COL.COLUMN_ID ASC
;