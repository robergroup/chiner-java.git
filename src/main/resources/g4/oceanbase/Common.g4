grammar Common;

INT:[0-9]+;
NAME: [a-z_A-Z0-9-\u4e00-\u9fa5]+;
SE:['`];
//COMMENTS:['`]?[a-z_A-Z0-9-\u4e00-\u9fa5(;,)（）/]+['`]?;
BLANK:' '+ ->skip; //忽略空格
NEWLINE: ('\n'|'\r')+ ->skip; //忽略换行
ANNOTATITION: ('--'|'//'|'#'|'/**'|('/*' .*?  '*/')) ~('\n'|'\r')* '\r'? '\n' -> skip;


/*** 名称 */
tenant_name:SE? NAME SE?;
pool_name:NAME;
unit_name:NAME;
zone_name:NAME;
region_name:NAME;

database_name:SE? NAME SE?;
table_name:SE? NAME SE?;
table_alias_name:NAME;
column_name:SE? NAME SE?;
column_alias_name :NAME;
partition_name:NAME;
subpartition_name:NAME;

index_name:SE? NAME SE?;
view_name:NAME;
object_name:NAME;
constraint_name:NAME;
tablegroup_name:NAME;

outline_name:NAME;
user_name:NAME;

table_factor:
    (database_name'.')? table_name;
column_factor:
    (table_factor'.')? column_name;


/** 表达式 */
expression:
    constant_value|
    column_factor |
    operator_expression|
    function_expression;

constant_value:
    INT | INT'.'INT | NAME;

operator_expression:
    INT (('*'|'+'|'-'|'/'|'//'|'%') INT)*
    ;
function_expression:NAME'('NAME(','NAME)*')';

/** 分布式 */
primary_zone:('PRIMARY_ZONE' | 'primary_zone') ('=')? zone_name;
zone_list:('ZONE_LIST' | 'zone_list') ('=')? '('zone_name*')';
replica_num:('REPLICA_NUM'|'replica_num') ('=')? INT;
tablegroup:default_tablegroup | table_tablegroup;
default_tablegroup:('DEFAULT'|'default') ('TABLEGROUP'|'tablegroup') ('=') tablegroup_name;
table_tablegroup:('TABLEGROUP'|'tablegroup') ('=')? tablegroup_name;

/** 部分建表的语句 */
index_desc:
    '('column_name (',' column_name)*')' index_type? ','?
    ;
index_type:('USING' 'BTREE')|('using' 'btree');
charset_name:SE? NAME SE?;
character_name:SE? NAME SE?;
collation_name:SE? NAME SE?;
logging_name:SE? NAME SE?;




