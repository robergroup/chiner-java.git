grammar OBCreate;
import OBDataType,Common,SqlServerComment;

prog:
    (databaseDDL
    |databaseUse
    |ddl_table
    |ddl_primary_key
    |ddl_index
    |filter_index
    )+;

databaseDDL:
    ('CREATE '|'create ') TEMPORARY? ('DATABASE '|'database ') (('IF '|'if ') ('NOT '|'not ') ('EXIST '|'exist '))? database_name ';';
databaseUse:
    ('USE'|'use') database_name ';';

ddl_table:
    ('CREATE '|'create ') TEMPORARY? ('TABLE '|'table ') (('IF '|'if ') ('NOT '|'not ') ('EXIST '|'exist '))? ('['tablegroup_name'].')? '['?table_name']'?'('
    table_definition_list')' table_option_list?
    tableDesc* comment? ';'
    | ('DROP '|'drop ') ('TABLES '|'tables '|('TABLE '|'table ')) (('IF '|'if ') ('EXISTS '|'exists '))? table_name+ ';'
    ;

ddl_primary_key:
      ('ALTER '|'alter ') ('TABLE '|'table ') table_name ('ADD '|'add ') columnDesc '('column_name_list')' ';'
    | ('ALTER '|'alter ') ('TABLE '|'table ') table_name ('DROP '|'drop ') columnDesc ',' ('ADD '|'add ') columnDesc '('column_name_list')'';'
    | ('ALTER '|'alter ') ('TABLE '|'table ') table_name ('DROP '|'drop ') columnDesc
;

ddl_index:
    ('CREATE '|'create ') ('UNIQUE INDEX '|('INDEX '|'index ')) index_name ('ON '|'on ') table_name '('column_name_list')' ';'
    | ('ALTER '|'alter ') ('TABLE '|'table ') table_name ('ADD '|'add ') ('INDEX '|'index ') index_name '('column_name_list')' ';'
    | ('DROP '|'drop ') ('INDEX '|'index ') index_name ('ON '|'on ') table_name ';'
    | ('ALTER '|'alter ') ('TABLE '|'table ') table_name ('DROP '|'drop ') ('INDEX '|'index ') index_name ';'
    | ('ALTER '|'alter ') ('TABLE '|'table ') table_name ('RENAME '|'rename ') ('INDEX '|'index ') index_name ('TO '|'to ') index_name ';'
;

filter_index:
    ('ALTER '|'alter ') ('INDEX '|'index ') index_name logging_name ';'
;

table_definition_list:
    table_definition (',' table_definition)*
    ;

table_definition:
    column_definition
    | (('CONTRAINT '|'contraint ') constraint_name)? columnDesc index_desc
    | (('CONTRAINT '|'contraint ') constraint_name)? columnDesc index_name index_desc
    ;

column_definition_list:
    column_definition (',' column_definition)*
    ;

column_definition
    : column_name data_type columnDesc* comment? charset? collate?
    | column_name data_type GENERATE_ALWAYS? ('AS '|'as ') expression ('VIRTUAL '|'virtual '|'STORED '|'stored ')? uniqueKey?
    | column_name data_type? GENERATE_ALWAYS? ('AS '|'as ') '(' expression ')' ('VIRTUAL '|'virtual '|'STORED '|'stored ')? uniqueKey?
    ;

table_option_list:
    table_option+
    ;

table_option:
    ('DEFAULT '|'default ') ('CHARSET '|'charset ' | 'CHARACTER ' 'SET '|'character ' 'set ') '=' charset_name
    | ('DEFAULT '|'default ') ('COLLATE '|'collate ') '=' collation_name
    | table_tablegroup
    | comment
    | ('DUPLICATE_SCOPE '|'duplicate_scope ') '=' 'none '|'NONE '|'CLUSTER '|'cluster '
    ;

column_name_list:
    column_name (',' column_name)*
    ;

comments:
    ('COMMENT'|'comment') ('ON '|'on ') ('TABLE '|'table ') table_name ('IS '|'is ') comment_value ';'
    |('COMMENT'|'comment') ('ON '|'on ') ('COLUMN '|'column ') table_name'.'column_name ('IS '|'is ') comment_value ';'
    | sqlServerCommonts
;

columnDesc:
    UNSIGNED|autoIncrement|primaryKey|uniqueKey|character|collate|charset|default;
autoIncrement:('autoIncrement'|'auto_increment'|'AUTO_INCREMENT') |'IDENTITY'|'identity'|'IDENTITY('INT')'|'identity('INT')'|'IDENTITY('INT','INT')'|'identity('INT','INT')';
notNull: ('NOT NULL'|'not null');
null:('NULL'|'null');
primaryKey:(('PRIMARY '|'primary ')? ('KEY'|'key'));
uniqueKey:('UNIQUE '|'unique ') (('KEY'|'key')|('INDEX '|'index '))?;
GENERATE_ALWAYS:('GENERATE '|'generate ') ('ALWAYS'|'always');
TEMPORARY:'TEMPORARY'|'temporary';
character:('CHARACTER SET'|'character set') character_name;
collate:('COLLATE '|'collate ') collation_name;
charset:('CHARSET '|'charset ') charset_name;
UNSIGNED:'UNSIGNED'|'unsigned';
default:('DEFAULT '|'default ')? (comment_value | null | notNull);

tableDesc:
    engine|tableCharset|tableCollate|tableAutoIncrement|rowFormat;
engine:('ENGINE'|'engine') '=' NAME;
rowFormat: ('ROW_FORMAT'|'row_format') '=' NAME;
tableAutoIncrement:autoIncrement '=' INT;
tableCharset:('DEFAULT '|'default ')? ('CHARSET'|'charset') '=' NAME;
tableCollate:('DEFAULT '|'default ')? ('COLLATE'|'collate') '=' NAME;


/** 注释 这个只能放这个文件，放Common一直报错 */
comment:('COMMENT'|'comment') ('=')? comment_value;
comment_value: (SE? (NAME|INT|','|';'|'；'|'，'|'/'|'.'|'。'|'('|')'|'（'|'）'|'-'|'——'|'"'|'“'|'”'|'NULL'|'null'|'%'|'['|']')+ SE?) | INT;
//comment_value:('('|'（')? 'NAME';
//ANNOTATItion1: '/*' .*?  '*/' -> skip; 这个兼容不行，放到java代码里面做

