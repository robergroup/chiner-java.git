package cn.com.chiner.java.antlr4.sql.oceanbase;

import cn.com.chiner.java.antlr4.sql.oceanbase.gen.OBCreateBaseVisitor;
import cn.com.chiner.java.antlr4.sql.oceanbase.gen.OBCreateParser;
import cn.com.chiner.java.antlr4.utils.Trim;
import cn.com.chiner.java.model.ColumnField;
import cn.com.chiner.java.model.TableEntity;
import cn.com.chiner.java.model.TableIndex;
import cn.com.chiner.java.model.TableIndexColumnField;
import com.google.common.base.Strings;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.h2.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 作者 : wantyx
 * 创建时间 : 2023/9/18
 * 实现功能 : 兼容sql写法，包含sqlserver的comment解析
 * todo：sqlServer的索引构建方式
 */
public class OceanBaseVisitorImpl extends OBCreateBaseVisitor<TableEntity> {
    private static final int FIRST = 0;
    private static final int SECOND = 1;
    private static final int THIRD = 2;

    private Map<String, TableEntity> tableMap = new HashMap<>();
    private Map<String, ColumnField> columnMap = new HashMap<>();
    private TableEntity curTable = new TableEntity();

    public Map<String, TableEntity> getTableMap() {
        return tableMap;
    }

    public void setTableMap(Map<String, TableEntity> tableMap) {
        this.tableMap = tableMap;
    }

    @Override
    protected TableEntity defaultResult() {
        return  this.curTable;
    }

    @Override
    public TableEntity visitDdl_table(OBCreateParser.Ddl_tableContext ctx) {
        TableEntity tableEntity = super.visitDdl_table(ctx);
        String tableName = tableEntity.getDefKey();
        this.tableMap.put(tableName,tableEntity);
        this.curTable = new TableEntity();
        this.columnMap = new HashMap<>();
        return tableEntity;
    }

    @Override
    public TableEntity visitComment(OBCreateParser.CommentContext ctx) {
        if (ctx.comment_value() != null){
            String tableComment = Trim.trimName(ctx.comment_value().getText());
            this.curTable.setDefName(tableComment);//表名的注释即为表中文名称
        }
        return super.visitComment(ctx);
    }

    @Override
    public TableEntity visitComments(OBCreateParser.CommentsContext ctx) {
        //判断comment类型
        int ruleIndex = FIRST;
        if (Objects.nonNull(ctx.column_name())){
            ruleIndex = SECOND;
        }
        if (Objects.nonNull(ctx.sqlServerCommonts())){
            ruleIndex = THIRD;
        }
        if (FIRST == ruleIndex){//comment on table
            String tableName = Trim.trimName(ctx.table_name().NAME().getText());
            if (tableMap.get(tableName) != null){
                TableEntity tableEntity = tableMap.get(tableName);
                tableEntity.setDefName(Trim.trimName(ctx.comment_value().getText()));
            }else {
                this.curTable.setDefName(Trim.trimName(ctx.comment_value().getText()));
            }
        }else if (SECOND == ruleIndex){ //comment on column
            String tableName = Trim.trimName(ctx.table_name().NAME().getText());
            String columnName = Trim.trimName(ctx.column_name().NAME().getText());
            if (tableMap.containsKey(tableName)){
                TableEntity tableEntity = tableMap.get(tableName);
                List<ColumnField> fields = tableEntity.getFields();
                for (ColumnField field : fields) {
                    if (field.getDefKey().equals(columnName)) {
                        field.setDefName(Trim.trimName(ctx.comment_value().getText()));
                        break;
                    }
                }
            }else {
                ColumnField columnField = columnMap.get(ctx.column_name().getText());
                columnField.setDefName(Trim.trimName(ctx.comment_value().getText()));
            }
        }else {//把sqlserver的commnet放到这里来，一并处理了算了
            OBCreateParser.SqlServerCommontsContext sqlServerCommontsContext = ctx.sqlServerCommonts();
            OBCreateParser.SqlServerCommentDescContext commentDescContext = sqlServerCommontsContext.sqlServerCommentDesc();
            String comment = Trim.trimName(commentDescContext.sqlServer_comment().getText());
            OBCreateParser.SqlServerTableDescContext tableDescContext = sqlServerCommontsContext.sqlServerTableDesc();
            String tableName = Trim.trimName(tableDescContext.table_name().getText());
            OBCreateParser.SqlServerColumnDescContext sqlServerColumnDescContext = sqlServerCommontsContext.sqlServerColumnDesc();
            String temp = Objects.isNull(sqlServerColumnDescContext.column_name()) ? "" : sqlServerColumnDescContext.column_name().getText();
            String columnName = Trim.trimName(temp);
            if (StringUtils.isNullOrEmpty(columnName)){ // 表comment
                TableEntity tableEntity = tableMap.getOrDefault(tableName, this.curTable);
                tableEntity.setDefName(comment);
            }else { //column comment
                TableEntity tableEntity = tableMap.getOrDefault(tableName, this.curTable);
                List<ColumnField> fields = tableEntity.getFields();
                for (ColumnField field : fields) {
                    if (columnName.equals(field.getDefKey())) {
                        field.setDefName(comment);
                        break;
                    }
                }
            }
        }
        return super.visitComments(ctx);
    }

    @Override
    public TableEntity visitColumn_definition(OBCreateParser.Column_definitionContext ctx) {
        TableEntity tableEntity = super.visitColumn_definition(ctx);
        int ruleIndex = ctx.getAltNumber();
        if (FIRST == ruleIndex){
            String columnName = Trim.trimName(ctx.column_name().NAME().getText());
            ColumnField columnField = columnMap.getOrDefault(columnName, new ColumnField());
            columnField.setDefKey(columnName);
            List<OBCreateParser.ColumnDescContext> columnDescContexts = ctx.columnDesc();
            for (OBCreateParser.ColumnDescContext columnDescContext : columnDescContexts) {
                if (columnDescContext.primaryKey() != null){
                    columnField.setPrimaryKey(true);
                }
                if (columnDescContext.autoIncrement() != null){
                    columnField.setAutoIncrement(true);
                }
                if (columnDescContext.default_() != null){
                    if(columnDescContext.default_().notNull()!=null){
                        columnField.setNotNull(true);
                    }
                    if (columnDescContext.default_().null_() != null){
                        columnField.setNotNull(false);
                    }
                    if (columnDescContext.default_().comment_value() != null){
                        //注意：这里的comment_value()并不是注释，而是默认值
                        String text = columnDescContext.default_().comment_value().getText();
                        String defaultValue = Trim.trimName(text);
                        if ("NULL".equalsIgnoreCase(defaultValue)){
                            columnField.setNotNull(false);
                        }else{
                            columnField.setDefaultValue(defaultValue);
                        }
                    }
                }
            }
//            for (TerminalNode terminalNode : columnDescContexts) {
//                String string = terminalNode.toString();
//                if (string.contains("NOT") && string.contains("NULL")){
//                    columnField.setNotNull(true);
//                }
//                if (string.contains("NULL") && !string.contains("NOT")){
//                    columnField.setNotNull(false);
//                }
//                if (string.contains("PRIMARY")){
//                    columnField.setPrimaryKey(true);
//                }
//                if (string.contains("INCREMENT")){
//                    columnField.setAutoIncrement(true);
//                }
//            }
            String dataType = ctx.data_type().children.get(0).toString();
            //分割dataType的数据类型+长度
            if (dataType.contains("(")){
                int i = dataType.indexOf("(");
                columnField.setType(dataType.substring(0,i));
                String length = dataType.substring(i + 1, dataType.length() - 1);
                String[] split = length.split(",");
                columnField.setLen(Integer.parseInt(split[0]));
                if (split.length>1){
                    columnField.setScale(Integer.parseInt(split[1]));
                }
            }else{
                columnField.setType(dataType);
            }
            if (Objects.nonNull(ctx.comment())){
                String comment = Trim.trimName(ctx.comment().comment_value().getText());
                columnField.setDefName(comment);
            }
//            if (Objects.nonNull(ctx.c())){
//                String defaultValue = Trim.trimName(ctx.comment_value().getText());
//                columnField.setDefaultValue(defaultValue);
//            }
            if (!columnMap.containsKey(columnName)){
                tableEntity.getFields().add(columnField);
            }
            columnMap.put(columnName,columnField);
        }
        return tableEntity;
    }

    @Override
    public TableEntity visitTable_name(OBCreateParser.Table_nameContext ctx) {
        TableEntity tableEntity = super.visitTable_name(ctx);
        tableEntity.setDefKey(Trim.trimName(ctx.NAME().getText()));
        return tableEntity;
    }

    @Override
    public TableEntity visitDdl_index(OBCreateParser.Ddl_indexContext ctx) {
        String tableName = Trim.trimName(ctx.table_name().NAME().getText());
        List<OBCreateParser.Column_nameContext> columnNameContexts = ctx.column_name_list().column_name();
        List<TableIndexColumnField> columnFields = columnNameContexts.stream()
                .map(columnNameContext -> Trim.trimName(columnNameContext.NAME().getText()))
                .map(s -> {
                    TableIndexColumnField tableIndexColumnField = new TableIndexColumnField();
                    tableIndexColumnField.setFieldDefKey(s);
                    return tableIndexColumnField;
                })
                .collect(Collectors.toList());
        TableEntity tableEntity = null;
        if (tableMap.containsKey(tableName)){
            tableEntity = tableMap.get(tableName);
        }else {
            tableEntity = this.curTable;
        }
        TableIndex tableIndex = new TableIndex();
        tableIndex.setDefKey(Trim.trimName(ctx.index_name(0).NAME().getText()));
        tableIndex.setFields(columnFields);
        tableEntity.getIndexes().add(tableIndex);
        return tableEntity;
    }
}
