package cn.com.chiner.java.antlr4.chain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 作者 : wantyx
 * 创建时间 : 2023/10/19
 * 实现功能 :
 */
public class SchemaIgnoreProcess extends SqlProcess {
    public static Logger logger = LoggerFactory.getLogger(SchemaIgnoreProcess.class);
    @Override
    public String apply(String sql){
        String regex ="(?i)\\buse\\s+\\w+;\\s*"; //直接将useF开头的这种语句干掉
        Pattern compile = Pattern.compile(regex);
        Matcher matcher = compile.matcher(sql);
        sql = matcher.replaceAll(" ");
        return process(sql);
    }
}
