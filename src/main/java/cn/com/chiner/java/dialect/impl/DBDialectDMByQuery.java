/*
 * Copyright 2019-2029 FISOK(www.fisok.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.com.chiner.java.dialect.impl;

import cn.com.chiner.java.dialect.DBDialect;
import cn.com.chiner.java.model.ColumnField;
import cn.com.chiner.java.model.TableEntity;
import cn.fisok.raw.kit.JdbcKit;
import cn.fisok.raw.kit.StringKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2021/6/19
 * @desc : 达梦数据库方言
 */
public class DBDialectDMByQuery extends DBDialectMetaByQuery {
    private static Logger logger = LoggerFactory.getLogger(DBDialectDMByQuery.class);

    @Override
    protected String getQueryTablesSQL() {
        return "SELECT\n" +
                "    t.table_name AS tbl_name,\n" +
                "    c.comments AS tbl_comment,\n" +
                "    t.OWNER AS db_name\n" +
                "FROM all_tables t left join all_tab_comments c on t.OWNER=c.OWNER and t.TABLE_NAME=c.TABLE_NAME\n" +
                "where t.OWNER=? AND t.table_name not like '##%'";
    }

    @Override
    protected String getQueryTableColumnsSQL() {
        return "SELECT\n" +
                "    col.table_name AS tbl_name,\n" +
                "    '' AS tbl_comment,\n" +
                "    col.column_name AS col_name,\n" +
                "    clc.comments AS col_comment,\n" +
                "    col.data_type AS data_type,\n" +
                "    col.data_length as data_length,\n" +
                "    col.data_precision AS data_precision,\n" +
                "    col.data_scale AS data_scale,\n" +
                "    col.nullable AS is_nullable,\n" +
                "    cpk.constraint_type AS is_primary_key,\n" +
                "    col.data_default AS default_value\n" +
                "FROM\n" +
                "    all_tab_columns col\n" +
                "        LEFT JOIN all_col_comments clc ON col.owner = clc.owner AND col.table_name = clc.table_name  AND col.column_name = clc.column_name\n" +
                "        LEFT JOIN (select cst.owner,cst.constraint_name,csc.table_name,csc.column_name,cst.constraint_type\n" +
                "                   from all_constraints cst,all_cons_columns csc\n" +
                "                   where 1=1\n" +
                "                     and cst.owner = csc.owner\n" +
                "                     and cst.constraint_name=csc.constraint_name\n" +
                "                     and   cst.constraint_type ='P'\n" +
                "                     and csc.table_name not like '%$%'\n" +
                "    ) cpk on col.owner = cpk.owner and col.table_name = cpk.table_name  AND col.column_name = cpk.column_name\n" +
                "WHERE\n" +
                "        col.OWNER = ?\n" +
                "  AND UPPER(col.table_name) = ?\n" +
                "ORDER BY COL.COLUMN_ID ASC";
    }

    @Override
    public TableEntity createTableEntity(Connection conn, DatabaseMetaData meta, String tableName, String schema) throws SQLException {
        String sql = getQueryTableColumnsSQL();
        logger.debug(sql);

        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, schema);
        pstmt.setString(2, tableName);


        TableEntity tableEntity = new TableEntity();
        tableEntity.setDefKey(tableName);

        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            tableEntity.setDefKey(rs.getString("tbl_name"));
            tableEntity.setComment(rs.getString("tbl_comment"));
            ColumnField field = new ColumnField();
            tableEntity.getFields().add(field);
            field.setDefKey(rs.getString("col_name"));
            field.setDefName(rs.getString("col_comment"));

            String dataType = rs.getString("data_type");
            Integer dataLength = rs.getInt("data_length");
            Integer numScale = rs.getInt("data_scale");
            if(withoutLenDataTypeName(dataType))
            {
                dataLength = null;
                numScale = null;
            }
            String isNullable = rs.getString("is_nullable");//  Y|N
            String isPrimaryKey = rs.getString("is_primary_key");//
            String defaultValue = rs.getString("default_value");//

            //数据类型以及长度
            field.setType(dataType);
            if (numScale != null && numScale > 0) {
                field.setLen(dataLength);
                if (numScale != null && numScale > 0) {
                    field.setScale(numScale);
                }
            } else if (dataLength != null && dataLength > 0) {
                field.setLen(dataLength);
            }
            field.setNotNull("N".equals(isNullable));
            field.setPrimaryKey("P".equals(isPrimaryKey));
//            if (dataType.toLowerCase().indexOf("char") >= 0) {
            if (defaultValue != null
                    && !defaultValue.startsWith("'")
                    && isStringDataType(dataType) ) {
                defaultValue = "'" + defaultValue + "'";
            }
            field.setDefaultValue(defaultValue);
        }

        JdbcKit.close(pstmt);
        JdbcKit.close(rs);

        return tableEntity;
    }
}
