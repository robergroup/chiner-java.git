package cn.com.chiner.java.command.impl.excel;

import cn.com.chiner.java.model.StandardField;
import cn.com.chiner.java.model.StandardFieldModule;
import cn.fisok.raw.kit.StringKit;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.ArrayList;
import java.util.List;

public class ParseStandardField {
    private List<StandardFieldModule> standardFieldModules = null;

    public List<StandardFieldModule> getStandardFieldModules() {
        return standardFieldModules;
    }

    public void parse(Workbook workbook){
        standardFieldModules = new ArrayList<>();
        //读取sheet
        Sheet sheet = workbook.getSheetAt(0);
        int lastRowNum = sheet.getLastRowNum();
        //行数小于2代表字段没有内容，无需解析
        if (lastRowNum >= 2) {
            //获取按组分析后的主题域对象列表
            standardFieldModules = parseStandardFieldWithGroupModel(sheet);
        }
    }

    /**
     * 解析分组模式下的标准字段库
     * @param catalogSheet 页清单
     */
    private List<StandardFieldModule> parseStandardFieldWithGroupModel(Sheet catalogSheet){
        //初始化最终的字段库模板对象集合
        List<StandardFieldModule> finalStandardFieldModules=new ArrayList<>();
        StandardFieldModule standardFieldModule =null;
        for(int i=2;i<=catalogSheet.getLastRowNum();i++){
            Row row = catalogSheet.getRow(i);
            Cell moduleDefKeyAndNameCell = row.getCell(0);
            Cell standardFieldDefKeyCell= row.getCell(1);
            String moduleDefKeyAndNameText=ExcelCommonUtils.getCellValue(moduleDefKeyAndNameCell).strValue("");
            String standardFieldDefKeyText=ExcelCommonUtils.getCellValue(standardFieldDefKeyCell).strValue("");
            //1处理标准字段库模板对象层
            if (StringKit.isNotEmpty(moduleDefKeyAndNameText)) {
                standardFieldModule = new StandardFieldModule();
                //1.1填充主题域名，主题域key
                fillStandardFieldModuleKeyAndName(standardFieldModule, moduleDefKeyAndNameText);
                //1.2填充主题id
                standardFieldModule.setId(StringKit.uuid("-").toUpperCase());
                //1.3初始化字段对象集合
                standardFieldModule.setFields(new ArrayList<>());
                //1.4加入最终解析完成的集合
                finalStandardFieldModules.add(standardFieldModule);
            }

            //2处理标准字段库字段对象层
            if (StringKit.isEmpty(standardFieldDefKeyText)) {
                continue;
            }
            //2.1解析行数据
            StandardField standardField = parseRowToStandardField(row);
            //2.2将对象信息添加到主题域对象中
            if (standardFieldModule != null) {
                standardFieldModule.getFields().add(standardField);
            }
        }
        return finalStandardFieldModules;

    }

    /**
     * 根据主题域名称填充标准字段库模板对象的key和name
     * @param standardFieldModule
     * @param moduleDefKeyAndNameText
     */
    private void fillStandardFieldModuleKeyAndName(StandardFieldModule standardFieldModule,String moduleDefKeyAndNameText){
            moduleDefKeyAndNameText = moduleDefKeyAndNameText.replaceAll("\\s+","");
            int linePos = moduleDefKeyAndNameText.indexOf("-");
            String key = linePos>1?moduleDefKeyAndNameText.substring(0,linePos):moduleDefKeyAndNameText;
            String name = linePos>1?moduleDefKeyAndNameText.substring(linePos+1):moduleDefKeyAndNameText;
            standardFieldModule.setDefKey(key);
            standardFieldModule.setDefName(name);
    }

    /**
     * 解析行数据，转为标准字段库字段对象
     * @param currentRow
     * @return
     */
    private StandardField parseRowToStandardField(Row currentRow) {
        Cell fieldDefKeyCell = currentRow.getCell( 1);
        Cell fieldDefNameCell = currentRow.getCell(2);
        Cell fieldDataTypeCell = currentRow.getCell( 3);
        Cell fieldLenCell = currentRow.getCell( 4);
        Cell fieldScaleCell = currentRow.getCell(5);
        Cell fieldDefaultValueCell = currentRow.getCell(6);
        Cell fieldCommentCell = currentRow.getCell(7);

        Integer len = ExcelCommonUtils.getCellValue(fieldLenCell).intValue(null);
        Integer scale = ExcelCommonUtils.getCellValue(fieldScaleCell).intValue(null);
        StandardField standardField = new StandardField();
        standardField.setDefKey(ExcelCommonUtils.getCellValue(fieldDefKeyCell).strValue(""));
        standardField.setDefName(ExcelCommonUtils.getCellValue(fieldDefNameCell).strValue(""));
        standardField.setComment(ExcelCommonUtils.getCellValue(fieldCommentCell).strValue(""));
        standardField.setType(ExcelCommonUtils.getCellValue(fieldDataTypeCell).strValue(""));
        standardField.setLen(len);
        standardField.setScale(scale);
        standardField.setDefaultValue(ExcelCommonUtils.getCellValue(fieldDefaultValueCell).strValue(""));
        return standardField;
    }
}
