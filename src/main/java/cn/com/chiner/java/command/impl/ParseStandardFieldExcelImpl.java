package cn.com.chiner.java.command.impl;

import cn.com.chiner.java.command.Command;
import cn.com.chiner.java.command.ExecResult;
import cn.com.chiner.java.command.impl.excel.ExcelCommonUtils;
import cn.com.chiner.java.command.impl.excel.ParseStandardField;
import cn.com.chiner.java.model.StandardFieldModule;
import cn.fisok.raw.kit.JSONKit;
import cn.fisok.raw.kit.StringKit;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ParseStandardFieldExcelImpl implements Command<ExecResult> {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    public ExecResult exec(Map<String, String> params) throws IOException {
        //根据参数名获取指定的文件路径
        String excelFile = params.get("excelFile");
        File inFile = new File(excelFile);
        ExecResult ret = new ExecResult();
        //获取到对应的excel文件，工作簿文件
        Workbook workBook = ExcelCommonUtils.getWorkbook(inFile);
        //对文件进行解析
        ParseStandardField parseStandardField=new ParseStandardField();
        parseStandardField.parse(workBook);
        try {
            //获取解析后的模板内容
            List<StandardFieldModule> standardFieldModules=parseStandardField.getStandardFieldModules();
            ret.setBody(standardFieldModules);
            ret.setStatus(ExecResult.SUCCESS);
            System.out.println(JSONKit.toJsonString(ret, true));
        } catch (Exception e) {
            String message = e.getMessage();
            if (StringKit.isBlank(message)) {
                message = e.toString();
            }
            ret.setBody(message);
            ret.setStatus(ExecResult.FAILED);
            logger.error("", e);
        }
        return ret;
    }
}
