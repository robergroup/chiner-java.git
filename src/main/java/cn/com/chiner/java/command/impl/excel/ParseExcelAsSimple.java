package cn.com.chiner.java.command.impl.excel;

import cn.com.chiner.java.model.ColumnField;
import cn.com.chiner.java.model.TableEntity;
import cn.fisok.raw.kit.StringKit;
import org.apache.poi.ss.usermodel.*;

import java.util.List;

/**
 * @author : 杨松<yangsong158@qq.com>
 * @date : 2023/02/23
 * @desc : 传统的简单模式从EXCEL导入表结构
 * 从原来的逻辑里剥离出来
 */
public class ParseExcelAsSimple {
    public void parseToTableEntities(Workbook workBook, List<TableEntity> tableEntities){
        Sheet sheet = workBook.getSheetAt(0);   //读取目录sheet
        for(int i=2;i<sheet.getLastRowNum();i++){
            Row row = sheet.getRow(i);
            if(row == null){
                continue;
            }
            Cell defKeyCell = row.getCell(0);
            Cell defNameCell = row.getCell(1);
            Cell commentCell = row.getCell(2);
            if(defKeyCell == null){
                continue;
            }
            String defKey = ExcelCommonUtils.getCellValue(defKeyCell).strValue("");
            String defName = ExcelCommonUtils.getCellValue(defNameCell).strValue("");
            String comment = ExcelCommonUtils.getCellValue(commentCell).strValue("");
            if(StringKit.isBlank(defKey)){
                continue;
            }

            TableEntity entity = new TableEntity();
            entity.setDefKey(defKey);
            entity.setDefName(defName);
            entity.setComment(comment);
            entity.setRowNo(tableEntities.size()+1);
            tableEntities.add(entity);

            //如果设置了超链接，则通过超链接读取sheet名
            String sheetName = defKey;
            Hyperlink hyperlink = defKeyCell.getHyperlink();
            if(hyperlink != null){
                String linkAddr = hyperlink.getAddress();
                sheetName = ExcelCommonUtils.parseTableNameFromLinkAddress(linkAddr);
                if(StringKit.isEmpty(sheetName)){
                    sheetName = defKey;
                }
            }


            ExcelCommonUtils.parseTableEntity(4,0,workBook,sheetName,entity);
            entity.fillFieldsCalcValue();
        }
    }

}
