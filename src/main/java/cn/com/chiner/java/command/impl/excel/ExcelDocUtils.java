package cn.com.chiner.java.command.impl.excel;

import cn.com.chiner.java.model.ColumnField;
import cn.com.chiner.java.model.ColumnHeader;
import cn.com.chiner.java.model.TableEntity;
import cn.hutool.core.util.BooleanUtil;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import java.lang.reflect.Method;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExcelDocUtils {
    private static final String[] attrArr = {"attr1", "attr2", "attr3","attr4","attr5","attr6","attr7","attr8","attr9"};
    /**
     * 填充数据表目录至Sheet的链接，并返回新的sheetName
     *
     * @param workbook
     * @param defKeyCell
     * @param defKey
     */
    public static String fillEntityCatalogLink(String defKey, Workbook workbook, Cell defKeyCell) {
        String sheetName = defKey;
        //当sheet表名长度超过27后，压缩表名长度，并且补2位数字（使用36进制），可以容纳36*36=1296个同名表，够用了
        if (sheetName.length() > 27) {
            for(int i=1;i<36*36;i++){
                String newSheetName = sheetName.substring(0,27)+"_"+(new BigInteger("" + i).toString(36).toUpperCase());
                Sheet testSheet = workbook.getSheet(newSheetName);
                if(testSheet == null){
                    sheetName = newSheetName;
                    break;
                }
            }
        }
        Hyperlink hyperlink = workbook.getCreationHelper().createHyperlink(HyperlinkType.DOCUMENT);
        hyperlink.setAddress("'" + sheetName + "'!A1");
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont(); // 创建字体对象
        font.setUnderline(Font.U_SINGLE); // 设置下划线样式为单线
        font.setColor(IndexedColors.BLUE.getIndex());
        font.setFontName("宋体-简");
        cellStyle.setFont(font); // 将字体应用到样式中

        defKeyCell.setCellStyle(cellStyle);
        defKeyCell.setHyperlink(hyperlink);

        return sheetName;
    }

    public static void fillFieldsIntoSheet(TableEntity entity, Workbook workbook, String sheetName, CellStyle defaultCellStyle, List<ColumnHeader> columnHeaders) throws Exception {
        int sheetIndex = workbook.getNumberOfSheets() - 1;
        Sheet sheet = workbook.getSheet(sheetName);
        //如果sheet存在了，说明表名重复，该表已经存在了，则不要生成了
        if(sheet != null){
            return;
        }
        sheet = workbook.cloneSheet(2);
        workbook.setSheetName(sheetIndex + 1, sheetName);
        Row entityDefKeyRow = sheet.getRow(0);
        Row entityDefNameRow = sheet.getRow(1);
        ExcelCommonUtils.touchCell(entityDefKeyRow, 2, defaultCellStyle).setCellValue(entity.getDefKey());
        ExcelCommonUtils.touchCell(entityDefNameRow, 2, defaultCellStyle).setCellValue(entity.getDefName());

        List<ColumnField> fields = entity.getFields();
        if(fields == null || fields.isEmpty()){
            return;
        }
        //获取开启的扩展属性列
        List<ColumnHeader> columnHeaderList=columnHeaders.stream().filter(columnHeader -> Arrays.asList(attrArr)
                .contains(columnHeader.getRefKey()) && BooleanUtil.isTrue(columnHeader.getEnable()))
                .collect(Collectors.toList());
        //在列名那一行增加新的列名
        if (!columnHeaderList.isEmpty()){
            Row fieldNameRow=ExcelCommonUtils.touchRow(sheet,3);
            //获取表格样式
            CellStyle standardStyle=fieldNameRow.getCell(9).getCellStyle();
            for (int i=0;i<columnHeaderList.size();i++){
                //填充内容
                ExcelCommonUtils.touchCell(fieldNameRow,10+i,standardStyle).setCellValue(columnHeaderList.get(i).getValue());
            }
        }
        for (int j = 0; j < fields.size(); j++) {
            ColumnField field = fields.get(j);
            Row fieldRow = ExcelCommonUtils.touchRow(sheet, 4 + j);
            ExcelCommonUtils.touchCell(fieldRow, 0, defaultCellStyle).setCellValue(j + 1);
            ExcelCommonUtils.touchCell(fieldRow, 1, defaultCellStyle).setCellValue(field.getDefKey());
            ExcelCommonUtils.touchCell(fieldRow, 2, defaultCellStyle).setCellValue(field.getDefName());
            ExcelCommonUtils.touchCell(fieldRow, 3, defaultCellStyle).setCellValue(field.getType());
            Integer len = field.getLen();
            if (len != null && len > 0) {
                ExcelCommonUtils.touchCell(fieldRow, 4, defaultCellStyle).setCellValue(len);
                Integer scale = field.getScale();
                if (scale != null && scale > 0) {
                    ExcelCommonUtils.touchCell(fieldRow, 5, defaultCellStyle).setCellValue(scale);
                }
            }
            if (field.getPrimaryKey() == Boolean.TRUE) {
                ExcelCommonUtils.touchCell(fieldRow, 6, defaultCellStyle).setCellValue("√");
            }
            if (field.getNotNull() == Boolean.TRUE) {
                ExcelCommonUtils.touchCell(fieldRow, 7, defaultCellStyle).setCellValue("√");
            }
            ExcelCommonUtils.touchCell(fieldRow, 8, defaultCellStyle).setCellValue(field.getDefaultValue());
            ExcelCommonUtils.touchCell(fieldRow, 9, defaultCellStyle).setCellValue(field.getComment());
            for (int i=0;i<columnHeaderList.size();i++){
              Object attrName = getGetMethod(field ,columnHeaderList.get(i).getRefKey());
              ExcelCommonUtils.touchCell(fieldRow,10+i,defaultCellStyle).setCellValue(String.valueOf(attrName));
            }
        }
    }
    /**
     * 根据属性，获取get方法
     * @param ob 对象
     * @param name 属性名
     * @return
     * @throws Exception
     */
    public static Object getGetMethod(Object ob , String name)throws Exception{
        Method[] m = ob.getClass().getMethods();
        for(int i = 0;i < m.length;i++){
            if(("get"+name).toLowerCase().equals(m[i].getName().toLowerCase())){
                return m[i].invoke(ob);
            }
        }
        return null;
    }
}
