package cn.com.chiner.java.model;

import java.util.List;

public class StandardFieldModule {
    private String id;                   //Id
    private String defKey;          //主题域key
    private String defName;         //主题域name
    private List<StandardField> fields;//字段库字段对象集合

    public String getDefKey() {
        return defKey;
    }

    public void setDefKey(String groupDefKey) {
        this.defKey = groupDefKey;
    }

    public String getDefName() {
        return defName;
    }

    public void setDefName(String groupDefName) {
        this.defName = groupDefName;
    }

    public List<StandardField> getFields() {
        return fields;
    }

    public void setFields(List<StandardField> fields) {
        this.fields = fields;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
